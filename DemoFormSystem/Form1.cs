﻿using Petrojayaforminsert;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class formLogin : Form
    {
        public static string EmailID { get; set; }
        public static string Password { get; set; }
        public static string CompanyName { get; set; }
        public static string VesselName { get; set; }
        public static string Types { get; set; }
        public static string To { get; set; }
        public static string CC { get; set; }
        public formLogin()
        {
            InitializeComponent();

            //EmailForm emailForm = new  EmailForm();
            //emailForm.Tag = this;
            //emailForm.Show(this);
            //Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string Colname = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(txtUserName.Text) || !string.IsNullOrEmpty(txtPassword.Text))
                {
                    List<Dictionary<string, string>> lstcrew = Helper.GetCrewExcelData(@"C:\Users\User\Desktop\Crew Details.xlsx");
                    Dictionary<string, string> crew = new Dictionary<string, string>();

                    foreach (Dictionary<string, string> dictcrew in lstcrew)
                    {
                        if (dictcrew["email id"].ToString().Equals(txtUserName.Text) && dictcrew["password"].ToString().Equals(txtPassword.Text))
                            crew = dictcrew;
                    }

                    //Login_DataBase login_DataBase = new Login_DataBase();
                    //var lstpassword = login_DataBase[txtUserName.Text];
                    if (crew != null && !string.IsNullOrEmpty(crew["password"]))
                    {
                        EmailID = crew["email id"];
                        Password = crew["password"];
                        CompanyName = crew["company name"];
                        VesselName = crew["ship name"];
                        Types = crew["type"];
                        To = crew["to"];
                        CC = crew["cc"];


                        if (!string.IsNullOrEmpty(Types) && Types.Equals("Shore"))
                        {
                            FormNames dashBoard = new FormNames(lstcrew);
                            this.Hide();
                            dashBoard.Show();
                        }
                        if (!string.IsNullOrEmpty(Types) && Types.Equals("Ship"))
                        {
                            DashBoard dashBoard = new DashBoard();
                            this.Hide();
                            dashBoard.Show();
                        }
                    }
                    else
                    {
                        lblErrorMsg.ForeColor = Color.Red;
                        lblErrorMsg.Text = "The username or password is incorrect.";
                    }
                }
                else
                {
                    //FormNames dashBoard = new FormNames();
                    //this.Hide();
                    //dashBoard.Show();
                    lblErrorMsg.ForeColor = Color.Red;
                    lblErrorMsg.Text = "The username or password is incorrect.";
                }
            }
            catch (Exception Error) {
                lblErrorMsg.Text = "Unexcepted error occurs, please make sure username and password is correct or closing Crea table excel and Report excel.";
            }
        }
    }
}
