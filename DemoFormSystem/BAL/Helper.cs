﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EAGetMail;
using EASendMail;
using System.Net;
using System.Net.NetworkInformation;

namespace DemoFormSystem
{
    public sealed class Helper
    {
        public static bool Validated(string val)
        {
            try
            {
                decimal de = Convert.ToDecimal(val);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// To store dictionary value in NoonReports database
        /// </summary>
        /// <param name="dictNoonReport">Collection of Form Values as Dictionary </param>
        /// <returns></returns>
        public static Tuple<string> SavetoDB(Dictionary<string, dynamic> dictNoonReport, string ExcelPath)
        {
           // using (SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
            //{
              StringBuilder strData = new StringBuilder();

                //try
                //{
                  //  con.Open();
                //}
                //catch { throw new Exception("The internet connection is not established, please make sure the internet connections."); }

                //SqlCommand cmd = new SqlCommand("[SP_Insert_DemoNoonReport]", con);
                //cmd.CommandType = System.Data.CommandType.StoredProcedure;

                if (dictNoonReport.ContainsKey("ReportType") && !string.IsNullOrEmpty(dictNoonReport["ReportType"]))
                {
                    //cmd.Parameters.AddWithValue("@ReportType", dictNoonReport["ReportType"]);
                    strData.Append(dictNoonReport["ReportType"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@ReportType", DBNull.Value);
                    strData.Append(" ").Append(",");
                }

                if (dictNoonReport.ContainsKey("VoyageNo") && !string.IsNullOrEmpty(dictNoonReport["VoyageNo"]))
                {
                    //cmd.Parameters.AddWithValue("@VoyageNo", dictNoonReport["VoyageNo"]);
                    strData.Append(dictNoonReport["VoyageNo"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@VoyageNo", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictNoonReport["ShipName"]))
                {
                    //cmd.Parameters.AddWithValue("@ShipName", dictNoonReport["ShipName"]);
                    strData.Append(dictNoonReport["ShipName"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@ShipName", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("ReportDate") && !string.IsNullOrEmpty(dictNoonReport["ReportDate"]))
                {
                    //cmd.Parameters.AddWithValue("@ReportDate", Convert.ToDateTime(dictNoonReport["ReportDate"]));
                    strData.Append(dictNoonReport["ReportDate"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@ReportDate", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictNoonReport["CallSign"]))
                {
                    //cmd.Parameters.AddWithValue("@CallSign", dictNoonReport["CallSign"]);
                    strData.Append(dictNoonReport["CallSign"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@CallSign", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictNoonReport["Latitude"]))
                {
                    //cmd.Parameters.AddWithValue("@Latitude", dictNoonReport["Latitude"]);
                    strData.Append(dictNoonReport["Latitude"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@Latitude", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictNoonReport["Longitude"]))
                {
                    //cmd.Parameters.AddWithValue("@Longitude", dictNoonReport["Longitude"]);
                    strData.Append(dictNoonReport["Longitude"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@Longitude", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictNoonReport["NPTime"]))
                {
                    //cmd.Parameters.AddWithValue("@NPTime", dictNoonReport["NPTime"]);
                    strData.Append(dictNoonReport["NPTime"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@NPTime", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictNoonReport["Remarks"]))
                {
                    //cmd.Parameters.AddWithValue("@Remarks", dictNoonReport["Remarks"]);
                    strData.Append(dictNoonReport["Remarks"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@Remarks", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictNoonReport["VesselHeading"]))
                {
                    //cmd.Parameters.AddWithValue("@VesselHeading", dictNoonReport["VesselHeading"]);
                    strData.Append(dictNoonReport["VesselHeading"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@VesselHeading", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("CTime") && !string.IsNullOrEmpty(dictNoonReport["CTime"]))
                {
                    //cmd.Parameters.AddWithValue("@CTime", dictNoonReport["CTime"]);
                    strData.Append(dictNoonReport["CTime"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@CTime", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictNoonReport["Remarks1"]))
                {
                    //cmd.Parameters.AddWithValue("@Remarks1", dictNoonReport["Remarks1"]);
                    strData.Append(dictNoonReport["Remarks1"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@Remarks1", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictNoonReport["AverageSpeed_At_Noons"]))
                {
                    //cmd.Parameters.AddWithValue("@AverageSpeed_At_Noons", dictNoonReport["AverageSpeed_At_Noons"]);
                    strData.Append(dictNoonReport["AverageSpeed_At_Noons"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@AverageSpeed_At_Noons", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictNoonReport["Forvoyagesincelastfullaway"]))
                {
                    //cmd.Parameters.AddWithValue("@Forvoyagesincelastfullaway", dictNoonReport["Forvoyagesincelastfullaway"]);
                    strData.Append(dictNoonReport["Forvoyagesincelastfullaway"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@Forvoyagesincelastfullaway", DBNull.Value);
                    strData.Append(",");
                }


                if (dictNoonReport.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictNoonReport["ForMainEngine"]))
                {
                    //cmd.Parameters.AddWithValue("@MFOForMainEngine", dictNoonReport["ForMainEngine"]);
                    strData.Append(dictNoonReport["ForMainEngine"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MFOForMainEngine", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictNoonReport["ForBoiler"]))
                {
                    //cmd.Parameters.AddWithValue("@MFOForBoiler", dictNoonReport["ForBoiler"]);
                    strData.Append(dictNoonReport["ForBoiler"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MFOForBoiler", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictNoonReport["ForMainEngine1"]))
                {
                    //cmd.Parameters.AddWithValue("@MGOForMainEngine1", dictNoonReport["ForMainEngine1"]);
                    strData.Append(dictNoonReport["ForMainEngine1"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MGOForMainEngine1", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictNoonReport["ForAuxiliaryEngines"]))
                {
                    //cmd.Parameters.AddWithValue("@MGOForAuxiliaryEngines", dictNoonReport["ForAuxiliaryEngines"]);
                    strData.Append(dictNoonReport["ForAuxiliaryEngines"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MGOForAuxiliaryEngines", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictNoonReport["ForBoiler1"]))
                {
                    //cmd.Parameters.AddWithValue("@MGOForBoiler1", dictNoonReport["ForBoiler1"]);
                    strData.Append(dictNoonReport["ForBoiler1"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MGOForBoiler1", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictNoonReport["TotalMFO"]))
                {
                    //cmd.Parameters.AddWithValue("@TotalMFO", dictNoonReport["TotalMFO"]);
                    strData.Append(dictNoonReport["TotalMFO"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@TotalMFO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictNoonReport["TotalMGO"]))
                {
                    //cmd.Parameters.AddWithValue("@TotalMGO", dictNoonReport["TotalMGO"]);
                    strData.Append(dictNoonReport["TotalMGO"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@TotalMGO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MFO") && !string.IsNullOrEmpty(dictNoonReport["MFO"]))
                {
                    //cmd.Parameters.AddWithValue("@MFO", dictNoonReport["MFO"]);
                    strData.Append(dictNoonReport["MFO"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MFO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MGO") && !string.IsNullOrEmpty(dictNoonReport["MGO"]))
                {
                    //cmd.Parameters.AddWithValue("@MGO", dictNoonReport["MGO"]);
                    strData.Append(dictNoonReport["MGO"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@MGO", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictNoonReport["MainEnginesystem_Sump"]))
                {
                    //cmd.Parameters.AddWithValue("@LoMainEnginesystem_Sump", dictNoonReport["MainEnginesystem_Sump"]);
                    strData.Append(dictNoonReport["MainEnginesystem_Sump"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@LoMainEnginesystem_Sump", DBNull.Value);
                    strData.Append(",");
                }
                if (dictNoonReport.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictNoonReport["MainEnginesystem_StorageTank"]))
                {
                    //cmd.Parameters.AddWithValue("@LoMainEnginesystem_StorageTank", dictNoonReport["MainEnginesystem_StorageTank"]);
                    strData.Append(dictNoonReport["MainEnginesystem_StorageTank"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@LoMainEnginesystem_StorageTank", DBNull.Value);
                    strData.Append(",");
                }

                if (dictNoonReport.ContainsKey("IsUpdated") && !string.IsNullOrEmpty(dictNoonReport["IsUpdated"]))
                {
                    //cmd.Parameters.AddWithValue("@isUpdated", dictNoonReport["IsUpdated"]);
                    strData.Append(dictNoonReport["IsUpdated"]).Append(",");
                }
                else
                {
                    //cmd.Parameters.AddWithValue("@isUpdated", DBNull.Value);
                    strData.Append(" ").Append(",");
                }

                //cmd.Parameters.AddWithValue("@RID", System.Data.SqlDbType.Int);
                //cmd.Parameters["@RID"].Direction = System.Data.ParameterDirection.Output;

                //int NRID = 0;

                try
                {
                  //  cmd.ExecuteNonQuery();

                    ExportExcel.addRowExistingExcel(dictNoonReport, ExcelPath);

                    
                    //if(cmd.Parameters["@RID"] != null)
                    //{
                      //  NRID = Convert.ToInt32(cmd.Parameters["@RID"].Value);
                    //}

                    //if (Petro_NoonReports.isNoon)
                    //    ExportExcel.Export_NoonReport(dictNoonReport, ExcelPath);
                    //else
                    //    ExportExcel.addRowExistingExcel(dictNoonReport, ExcelPath);
                }
                catch (Exception er)
                {
                    throw new Exception(er.Message);
                }

                return new Tuple<string>(strData.ToString());
            //}
            return new Tuple<string>(string.Empty);

        }


        // store Excel sheet (or any file for that matter) into a SQL Server table  
        public static void StoreFileToDatabase(string FileName, int ReportID = 0)
        {
            // if file doesn't exist --> terminate (you might want to show a message box or something)  
            if (!File.Exists(FileName))
            {
                return;
            }

            getContentType getContentTypes = new getContentType();
            string contentType = getContentTypes[Path.GetExtension(FileName)];
            // get all the bytes of the file into memory  
            byte[] excelContents = File.ReadAllBytes(FileName);

            // define SQL statement to use  
            string insertStmt = "INSERT INTO ProjectExce(FileNames, ContentType, Data) VALUES(@FileName, @ContentType, @BinaryContent)";

            // set up connection and command to do INSERT  
            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
            {
                using (SqlCommand cmdInsert = new SqlCommand(insertStmt, connection))
                {
                    cmdInsert.Parameters.Add("@FileName", System.Data.SqlDbType.NVarChar, 500).Value = Path.GetFileName(FileName);
                    cmdInsert.Parameters.Add("@ContentType", System.Data.SqlDbType.NVarChar, 300).Value = contentType;
                    cmdInsert.Parameters.Add("@BinaryContent", System.Data.SqlDbType.VarBinary, int.MaxValue).Value = excelContents;

                    // open connection, execute SQL statement, close connection again  
                    connection.Open();
                    cmdInsert.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public static void RetrieveExcelFileFromDatabase(int ID, string excelFileName)
        {
            byte[] excelContents;

            string selectStmt = "SELECT Data FROM AttachFiles WHERE ID = @ID";

            using (SqlConnection connection = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
            using (SqlCommand cmdSelect = new SqlCommand(selectStmt, connection))
            {
                cmdSelect.Parameters.Add("@ID", System.Data.SqlDbType.Int).Value = 1;

                connection.Open();
                excelContents = (byte[])cmdSelect.ExecuteScalar();
                connection.Close();
            }

            File.WriteAllBytes(@"C:\Users\User\Desktop\Test.xlsx", excelContents);
        }

        public static string DecompressString(string compressedtext)
        {
            byte[] gZipBuffer = Convert.FromBase64String(compressedtext);
            using (var memoryStream = new MemoryStream())
            {
                int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                var buffer = new byte[dataLength];

                memoryStream.Position = 0;
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                {
                    gZipStream.Read(buffer, 0, buffer.Length);
                }

                return Encoding.UTF8.GetString(buffer);
            }
        }

        public static Tuple<List<ReportsGrid>, Dictionary<int, Dictionary<string, dynamic>>>  GetValuesfromExcel(string fileName)
        {
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, false))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        //SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        //SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.LastOrDefault();
                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> receiptField = new List<string>();
                        string ColName = string.Empty;

                        //receiptField.Add("ID");
                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = Helper.GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!receiptField.Contains("Voyage No"))
                        {
                            worksheetPart = workbookPart.WorksheetParts.First();
                            sheet = worksheetPart.Worksheet;
                            sheetData = sheet.GetFirstChild<SheetData>();
                            rows = sheetData.Descendants<Row>();
                            receiptField.Clear();
                            ColName = string.Empty;

                            //receiptField.Add("ID");
                            foreach (Cell cell in rows.ElementAt(0))
                            {
                                ColName = Helper.GetCellValue(doc, cell);
                                if (ColName.StartsWith(" "))
                                    ColName = ColName.Substring(1);
                                receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                            }

                            if (!receiptField.Contains("Voyage No"))
                                throw (new Exception("The local database not exist in the system, we not proceed further."));
                        }

                        List<ReportsGrid> lstgrid = new List<ReportsGrid>();

                        Dictionary<int, Dictionary<string,dynamic>> dict = new Dictionary<int, Dictionary<string, dynamic>>();

                        Dictionary<string, dynamic> dicNoonReport;

                        ReportsGrid reportsGrid;

                        string colValue = string.Empty;
                        int count = 0;

                        foreach (Row row in rows)
                        {
                            if ( row.RowIndex != 1)
                            {
                                count++;
                                dicNoonReport = new Dictionary<string, dynamic>();
                                reportsGrid = new ReportsGrid();
                                for (int i = 0, arrIndex = 0; i < receiptField.Count; i++, arrIndex++) ///row.Descendants<Cell>().Count() replaced
                                {
                                    colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));

                                    reportsGrid.ID = count;

                                    switch (receiptField[arrIndex])
                                    {
                                        //case "ID":
                                        //    dicNoonReport.Add("ID", count);
                                        //    break;

                                        case "Report Type":
                                            dicNoonReport.Add("ReportType", colValue);
                                            break;

                                        case "Is Updated":
                                            dicNoonReport.Add("IsUpdated", colValue);
                                            break;

                                        case "Voyage No":
                                            reportsGrid.VoyageNo = colValue;
                                            dicNoonReport.Add("VoyageNo", colValue);
                                            break;

                                        case "Ship Name":
                                            reportsGrid.ShipName = colValue;
                                            dicNoonReport.Add("ShipName", colValue);
                                            break;

                                        case "Report Date":
                                            reportsGrid.ReportDate = Convert.ToDateTime(colValue).Date;
                                            dicNoonReport.Add("ReportDate", Convert.ToDateTime(colValue).ToString("dd/MM/yyyy"));
                                            break;

                                        case "Call Sign":
                                            reportsGrid.CallSign = colValue;
                                            dicNoonReport.Add("CallSign", colValue);
                                            break;

                                        case "Latitude":
                                            dicNoonReport.Add("Latitude", colValue);
                                            break;

                                        case "Longitude":
                                            dicNoonReport.Add("Longitude", colValue);
                                            break;

                                        case "PositionTime at":
                                            try
                                            {
                                                TimeSpan time = TimeSpan.Parse(Convert.ToString(colValue));
                                                reportsGrid.PositionTime = time;
                                            }
                                            catch { }
                                            dicNoonReport.Add("NPTime", colValue);
                                            break;

                                        case "Position Remark":
                                            dicNoonReport.Add("Remarks", colValue);
                                            break;

                                        case "Vessel Heading":
                                            dicNoonReport.Add("VesselHeading", colValue);
                                            break;

                                        case "Course Time":
                                            dicNoonReport.Add("CTime", colValue);
                                            break;

                                        case "Course Remark":
                                            dicNoonReport.Add("Remarks1", colValue);
                                            break;

                                        case "Average Speed At Noons":
                                            dicNoonReport.Add("AverageSpeed_At_Noons", colValue);
                                            break;

                                        case "For Voyage since last full away":
                                            dicNoonReport.Add("Forvoyagesincelastfullaway", colValue);
                                            break;

                                        case "MFO For Main Engine":
                                            dicNoonReport.Add("ForMainEngine", colValue);
                                            break;

                                        case "MFO For Boiler":
                                            dicNoonReport.Add("ForBoiler", colValue);
                                            break;

                                        case "MGO Main Engine":
                                            dicNoonReport.Add("ForMainEngine1", colValue);
                                            break;

                                        case "MGO Auxiliary Engine":
                                            dicNoonReport.Add("ForAuxiliaryEngines", colValue);
                                            break;

                                        case "MGO Boiler":
                                            dicNoonReport.Add("ForBoiler1", colValue);
                                            break;

                                        case "Total MFO":
                                            dicNoonReport.Add("TotalMFO", colValue);
                                            break;

                                        case "Total MGO":
                                            dicNoonReport.Add("TotalMGO", colValue);
                                            break;

                                        case "Bunker Received MFO":
                                            dicNoonReport.Add("MFO", colValue);
                                            break;

                                        case "Bunker Received MGO":
                                            dicNoonReport.Add("MGO", colValue);
                                            break;

                                        case "LO Main Engine system Sump":
                                            dicNoonReport.Add("MainEnginesystem_Sump", colValue);
                                            break;

                                        case "LO Main Engine system Storage Tank":
                                            dicNoonReport.Add("MainEnginesystem_StorageTank", colValue);
                                            break;
                                    }  
                                }
                                dicNoonReport.Add("ID", count);
                                if(dicNoonReport["ReportType"].Equals("Noon"))

                                dict.Add(count, dicNoonReport);
                                lstgrid.Add(reportsGrid);
                            }
                        }

                        return new Tuple<List<ReportsGrid>, Dictionary<int, Dictionary<string, dynamic>>>(lstgrid, dict);
                    }
                }
            }
            catch (Exception erro)
            {
                throw (new Exception(erro.Message.ToString()));
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return cell.CellValue.InnerXml;
                }
            }
            catch { return cell.CellValue.InnerXml; }
        }


        public static List<Dictionary<string,string>> GetCrewExcelData(string fileName)
        {
            List<Dictionary<string, string>> lstdict = new List<Dictionary<string, string>>();
            try
            {
                using (FileStream fs = new FileStream(fileName, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        //SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        //SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.LastOrDefault();


                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> receiptField = new List<string>();
                        string ColName = string.Empty;

                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!receiptField.Contains("Crew Id"))
                        {
                            throw (new Exception("The database not in your system, please contact administrator."));
                        }

                        string colValue = string.Empty;

                        foreach (Row row in rows)
                        {
                            if (row.RowIndex != 1)
                            {
                                Dictionary<string, string> dict = new Dictionary<string, string>();
                                for (int i = 0, arrIndex = 0; i < row.Descendants<Cell>().Count(); i++, arrIndex++)
                                {
                                    colValue = GetCellValue(doc, row.Descendants<Cell>().ElementAt(i));


                                    
                                    if ((receiptField[arrIndex].ToLower().Equals("crew id")))
                                        dict.Add("crew id", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("crew name"))
                                        dict.Add("crew name", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("crew date of joining"))
                                        dict.Add("crew date of joining", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("crew title"))
                                        dict.Add("crew title", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("crew leaving date"))
                                        dict.Add("crew leaving date", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("access level"))
                                        dict.Add("access level", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("is suspension"))
                                        dict.Add("is suspension", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("username"))
                                        dict.Add("username", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("password"))
                                        dict.Add("password", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("email id"))
                                        dict.Add("email id", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("company name"))
                                        dict.Add("company name", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("ship name"))
                                        dict.Add("ship name", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("type"))
                                        dict.Add("type", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("to"))
                                        dict.Add("to", colValue);
                                    else if (receiptField[arrIndex].ToLower().Equals("cc"))
                                        dict.Add("cc", colValue);

                                    else
                                        break;

                                }

                                if (!string.IsNullOrEmpty(dict["email id"]))
                                    lstdict.Add(dict);
                            }
                        }

                        return lstdict;
                    }
                }
            }
            catch (Exception ex)
            {

                return lstdict;
                     
            }
        }

        public static string CompressedZIP(string filename, string withoutFN)
        {
            string outputfilename = Path.GetTempPath() + withoutFN + ".zip";

            using (FileStream inputstream = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite))

            using (FileStream outputstream = new FileStream(outputfilename, FileMode.OpenOrCreate, FileAccess.ReadWrite))
            {
                using (GZipStream csv = new GZipStream(outputstream, CompressionMode.Compress))
                {
                    inputstream.CopyTo(csv);
                }
            }
            return outputfilename;
        }

        public static string SendEmail( string mailid,string password,string to, string file, string subject, string body, List<string> ccmails, string otherfile,FileInfo AttachmentfileInfo=null)
        {
            try
            {
                SmtpMail oMail = new SmtpMail("TryIt");
                SmtpClient oSmtp = new SmtpClient();

                // Set sender email address, please change it to yours
                oMail.From = mailid;

                oMail.To = to;

                // Set email subject
                oMail.Subject = subject;

                // Set email body
                oMail.TextBody = body;

                // oMail.AddAttachment(file);

                if (string.IsNullOrEmpty(otherfile))
                {
                    //oMail.AddAttachment(file);

                }
                else
                {
                    // otherfile = txt_OtherAttachments.Text.ToString();

                    otherfile = AttachmentfileInfo.ToString();

                    oMail.AddAttachment(otherfile);

                }

                // Your SMTP server address
                SmtpServer oServer = new SmtpServer("mail.ldksatcom.in");
                // User and password for ESMTP authentication, if your server doesn't require
                // User authentication, please remove the following codes.
                oServer.User = mailid;

                oServer.Password = password;

                string[] arccmails = new string[5];

                foreach (string ccmail in ccmails)
                {
                    if (!string.IsNullOrEmpty(ccmail))
                    {
                        oMail.Cc.Add(ccmail);
                    }

                }

                // Set recipient email address, please change it to yours
                string[] artomail = new string[3];

                if (!string.IsNullOrEmpty(to))
                {
                    if (to.Contains(';'))
                    {
                        artomail = to.Split(';');
                    }
                    else if (to.Contains(','))
                    {
                        artomail = to.Split(',');
                    }
                    else
                        artomail[0] = to;
                }

                foreach (string tomail in artomail)
                {
                    if (!string.IsNullOrEmpty(tomail))
                    {
                        oMail.To.Add(tomail);

                    }
                }
                // If your smtp server requires TLS connection, please add this line
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                // If your smtp server requires implicit SSL connection on 465 port, please add this line
                //oServer.Port = 465;
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;
                try
                {
                    oSmtp.SendMail(oServer, oMail);
                    return "email was sent successfully!";
                    
                }
                catch (System.Exception ep)
                {
                    return "failed to send email with the following error:" + ep.Message;
                }
            }
            catch (System.Exception error)
            {
                return "failed to send email with the following error:" + error.Message;
            }




            #region
            //try
            //{
            //    Outlook.Application oApp = new Outlook.Application();

            //    Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

            //    Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;

            //    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(to);
            //    oRecip.Resolve();

            //    oMsg.CC = ccmails;

            //    //Add an attachment.
            //    if (file.Contains("|"))
            //    {
            //        string[] attached = file.Split('|');

            //        String sDisplayName = file;
            //        //int iPosition = (int)oMsg.Body.Length + 1;
            //        //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
            //        //now attached the file
            //        Outlook.Attachment oAttach;

            //        foreach (string att in attached)
            //        {
            //            oAttach = oMsg.Attachments.Add(file, Type.Missing, Type.Missing, Type.Missing);
            //        }
            //    }
            //    else
            //    {
            //        //String sDisplayName = file;
            //        //int iPosition = (int)oMsg.Body.Length + 1;
            //        //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
            //        //now attached the file
            //        Outlook.Attachment oAttach = oMsg.Attachments.Add
            //                                     (file, Type.Missing, Type.Missing, Type.Missing);
            //    }
            //    //Subject line
            //    oMsg.Subject = subject;
            //    // Send.
            //    oMsg.Send();

            //    MessageBox.Show("Email was successfully Send.", "Successs Message", MessageBoxButtons.OK);
            //    clearFields();
            //    printPDFWithAcrobat();
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            #endregion
        }

        public static bool checkInterner()
        {
            try
            {
                Ping myPing = new Ping();
                String host = "google.com";
                byte[] buffer = new byte[32];
                int timeout = 1000;
                PingOptions pingOptions = new PingOptions();
                PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                return (reply.Status == IPStatus.Success);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetIPAddress()
        {
            try
            {
                String address = string.Empty;
                string HostName = string.Empty;
                try
                {
                    WebRequest request = WebRequest.Create("http://checkip.dyndns.org/");
                    using (WebResponse response = request.GetResponse())
                    using (StreamReader stream = new StreamReader(response.GetResponseStream()))
                    {
                        address = stream.ReadToEnd();
                    }
                }
                catch (Exception ee) { return "error"; }

                int first = address.IndexOf("Address: ") + 9;
                int last = address.LastIndexOf("</body>");
                address = address.Substring(first, last - first);

                string IPAdd = address;
                Console.WriteLine("IP Address: " + address);

                try
                {
                    IPHostEntry hostEntry = Dns.GetHostEntry(IPAdd);
                    HostName = hostEntry.HostName;
                    string[] arrHost = HostName.Split('.');
                    int totalLength = arrHost.Length;
                    if (arrHost[totalLength - 1].ToString().Length == 2 || arrHost[totalLength - 1].ToString().Length == 3)
                    {
                        HostName = arrHost[totalLength - 2];
                    }

                    if (HostName.ToLower().Contains("inmarsat"))
                    {
                        HostName = "FleetBoard: " + HostName;
                    }
                    else if (HostName.ToLower().Contains("iridium"))
                    {
                        HostName = "FleetBoard: " + HostName;
                    }
                    else if (HostName.ToLower().Contains("thuraya"))
                    {
                        HostName = "FleetBoard: " + HostName;
                    }
                    else if (HostName.ToLower().Contains("Global"))
                    {
                        HostName = "FleetBoard: " + HostName;
                    }
                    else if (HostName.ToLower().Contains("intelsat"))
                    {
                        HostName = "FleetBoard: " + HostName;
                    }
                    else
                    {
                        HostName = "TCP/IP: " + HostName;
                    }
                }
                catch (Exception err)
                {
                    return "error";
                }
                return HostName;
            }
            catch (Exception err) { return "error"; }
        }
    }
}