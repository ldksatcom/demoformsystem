﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoFormSystem
{
    public sealed class ExportExcel
    {
        #region Store Local Excel DB

        private static List<string> GetColName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Report Type");
            lstCol.Add("Is Updated");
            lstCol.Add("Voyage No");
            lstCol.Add("Ship Name");
            lstCol.Add("Report Date");
            lstCol.Add("Call Sign");
            lstCol.Add("Latitude");
            lstCol.Add("Longitude");
            lstCol.Add("PositionTime at");
            lstCol.Add("Position Remark");
            lstCol.Add("Vessel Heading");
            lstCol.Add("Course Time");
            lstCol.Add("Course Remark");
            lstCol.Add("Average Speed At Noons");
            lstCol.Add("For Voyage since last full away");
            lstCol.Add("MFO For Main Engine");
            lstCol.Add("MFO For Boiler");
            lstCol.Add("MGO Main Engine");
            lstCol.Add("MGO Auxiliary Engine");
            lstCol.Add("MGO Boiler");
            lstCol.Add("Total MFO");
            lstCol.Add("Total MGO");
            lstCol.Add("Bunker Received MFO");
            lstCol.Add("Bunker Received MGO");
            lstCol.Add("LO Main Engine system Sump");
            lstCol.Add("LO Main Engine system Storage Tank");
            lstCol.Add("Attachments");

            return lstCol;
        }

        public static void SaveExcelDB_New(Dictionary<string, dynamic> dictFromData, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();

                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();


                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = "Noon Report" };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    System.Data.DataTable table = new System.Data.DataTable();
                    List<string> arColName = GetColName();

                    DocumentFormat.OpenXml.UInt32Value rowIndex = 0;

                    foreach (string column in arColName)
                    {
                        columns.Add(column);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column);
                        headerRow.AppendChild(cell);
                    }
                    rowIndex++;
                    headerRow.RowIndex = rowIndex;
                    sheetData.AppendChild(headerRow);

                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (string col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        switch (col)
                        {
                            case "Report Type":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ReportType"]);
                                newRow.AppendChild(cell);
                                break;

                            case "Is Updated":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["IsUpdated"]);
                                newRow.AppendChild(cell);
                                break;

                            case "Voyage No":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VoyageNo"]);
                                newRow.AppendChild(cell);
                                break;
                            case "Ship Name":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictFromData["ShipName"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ShipName"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Report Date":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(dictFromData["ReportDate"]).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                newRow.AppendChild(cell);
                                break;
                            case "Call Sign":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictFromData["CallSign"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CallSign"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Latitude":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictFromData["Latitude"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Latitude"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                newRow.AppendChild(cell);
                                break;
                            case "Longitude":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictFromData["Longitude"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Longitude"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                newRow.AppendChild(cell);
                                break;
                            case "PositionTime at":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictFromData["NPTime"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["NPTime"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                newRow.AppendChild(cell);
                                break;
                            case "Position Remark":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictFromData["Remarks"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Remarks"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Vessel Heading":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictFromData["VesselHeading"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VesselHeading"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "Course Time":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("CTime") && !string.IsNullOrEmpty(dictFromData["CTime"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CTime"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(string.Empty);
                                newRow.AppendChild(cell);
                                break;

                            case "Course Remark":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictFromData["Remarks1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["Remarks1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "Average Speed At Noons":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictFromData["AverageSpeed_At_Noons"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["AverageSpeed_At_Noons"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "For Voyage since last full away":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictFromData["Forvoyagesincelastfullaway"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Forvoyagesincelastfullaway"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "MFO For Main Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictFromData["ForMainEngine"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MFO For Boiler":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictFromData["ForBoiler"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Main Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictFromData["ForMainEngine1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Auxiliary Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictFromData["ForAuxiliaryEngines"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForAuxiliaryEngines"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Boiler":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictFromData["ForBoiler1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Total MFO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictFromData["TotalMFO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["TotalMFO"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Total MGO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictFromData["TotalMGO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["TotalMGO"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Bunker Received MFO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("MFO") && !string.IsNullOrEmpty(dictFromData["MFO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MFO"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Bunker Received MGO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("MGO") && !string.IsNullOrEmpty(dictFromData["MGO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MGO"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "LO Main Engine system Sump":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                if (dictFromData.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MainEnginesystem_Sump"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "LO Main Engine system Storage Tank":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                if (dictFromData.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_StorageTank"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MainEnginesystem_StorageTank"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("0");
                                newRow.AppendChild(cell);
                                break;
                            case "Attachments":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("Empty Attach");
                                newRow.AppendChild(cell);
                                break;
                        }
                    }
                    rowIndex++;
                    newRow.RowIndex = rowIndex;
                    sheetData.AppendChild(newRow);

                }
            }
            catch (Exception roor) { }
        }

        public static void addRowExistingExcel(Dictionary<string, dynamic> dictFromData, string destination)
        {
            try
            {
                if (!File.Exists(destination))
                {
                    SaveExcelDB_New(dictFromData, destination);
                    return;
                }

                using (FileStream fs = new FileStream(destination, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        //SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        //SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.LastOrDefault();


                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> receiptField = new List<string>();
                        string ColName = string.Empty;

                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!receiptField.Contains("Voyage No"))
                        {
                            SaveExcelDB_New(dictFromData, destination);
                        }
                        else
                            InsertReportRow(worksheetPart, dictFromData, receiptField);
                    }
                }
            }
            catch (Exception erro)
            {
                throw (new Exception(erro.Message.ToString()));
            }
        }

        public static void InsertReportRow(WorksheetPart worksheetPart, Dictionary<string, dynamic> dictFromData, List<string> columns)
        {
            SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            Row lastRow = sheetData.Elements<Row>().LastOrDefault();

            DocumentFormat.OpenXml.UInt32Value rowIndex = 0;
            if(lastRow.RowIndex != null)
            {
                rowIndex = lastRow.RowIndex;
            }
            if (lastRow != null)
            {
                DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                foreach (string col in columns)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    switch (col)
                    {
                        case "Report Type":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ReportType"]);
                            newRow.AppendChild(cell);
                            break;
                        case "Is Updated":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["IsUpdated"]);
                            newRow.AppendChild(cell);
                            break;

                        case "Voyage No":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VoyageNo"]);
                            newRow.AppendChild(cell);
                            break;
                        case "Ship Name":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictFromData["ShipName"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ShipName"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Report Date":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(dictFromData["ReportDate"]).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                            newRow.AppendChild(cell);
                            break;
                        case "Call Sign":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictFromData["CallSign"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CallSign"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Latitude":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictFromData["Latitude"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Latitude"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                            newRow.AppendChild(cell);
                            break;
                        case "Longitude":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictFromData["Longitude"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Longitude"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                            newRow.AppendChild(cell);
                            break;
                        case "PositionTime at":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictFromData["NPTime"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["NPTime"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Position Remark":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictFromData["Remarks"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Remarks"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Vessel Heading":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictFromData["VesselHeading"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VesselHeading"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Course Time":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("CTime") && !string.IsNullOrEmpty(dictFromData["CTime"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CTime"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Course Remark":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictFromData["Remarks1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["Remarks1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Average Speed At Noons":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictFromData["AverageSpeed_At_Noons"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["AverageSpeed_At_Noons"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "For Voyage since last full away":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictFromData["Forvoyagesincelastfullaway"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Forvoyagesincelastfullaway"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "MFO For Main Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictFromData["ForMainEngine"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MFO For Boiler":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictFromData["ForBoiler"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Main Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictFromData["ForMainEngine1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Auxiliary Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictFromData["ForAuxiliaryEngines"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForAuxiliaryEngines"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Boiler":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictFromData["ForBoiler1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Total MFO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictFromData["TotalMFO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["TotalMFO"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Total MGO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictFromData["TotalMGO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["TotalMGO"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Bunker Received MFO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("MFO") && !string.IsNullOrEmpty(dictFromData["MFO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MFO"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Bunker Received MGO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("MGO") && !string.IsNullOrEmpty(dictFromData["MGO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MGO"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "LO Main Engine system Sump":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                            if (dictFromData.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MainEnginesystem_Sump"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "LO Main Engine system Storage Tank":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                            if (dictFromData.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_StorageTank"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MainEnginesystem_StorageTank"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Attachments":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue("");
                            newRow.AppendChild(cell);
                            break;
                    }
                }
                rowIndex++;
                newRow.RowIndex = rowIndex;
                sheetData.AppendChild(newRow);
            }
            else
            {
                sheetData.InsertAt(new Row() { RowIndex = 0 }, 0);
            }
        }

        #endregion

        public static void addAttachfileToexcel(string destination, string attachFileName)
        {
            using (FileStream fs = new FileStream(destination, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
            {
                using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                {
                    WorkbookPart workbookPart = doc.WorkbookPart;
                    //SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                    //SharedStringTable sst = sstpart.SharedStringTable;

                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.LastOrDefault();


                    Worksheet sheet = worksheetPart.Worksheet;

                    SheetData sheetData = sheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    List<string> receiptField = new List<string>();
                    string ColName = string.Empty;

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        ColName = GetCellValue(doc, cell);
                        if (ColName.StartsWith(" "))
                            ColName = ColName.Substring(1);
                        receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                    }

                    SheetData sheetDatas = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                    Row lastRow = sheetData.Elements<Row>().LastOrDefault();

                   
                    if (lastRow != null)
                    {
                        int count= lastRow.Elements<Cell>().Count();
                        Cell cell = lastRow.Elements<Cell>().LastOrDefault();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(attachFileName);
                        
                        sheet.Save();
                    }

                }
            }
        }

        #region morning Report

        private static List<string> GetARColName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Voyage No");
            lstCol.Add("Ship Name");
            lstCol.Add("Report Date");
            lstCol.Add("Call Sign");
            lstCol.Add("Latitude");
            lstCol.Add("Longitude");
            lstCol.Add("Noon Position Time at");
            lstCol.Add("Noon Position Remark");
            lstCol.Add("Vessel Heading");
            lstCol.Add("Course Time");
            lstCol.Add("Course Remark");
            lstCol.Add("Average Speed At Noons");
            lstCol.Add("For Voyage since last full away");
            lstCol.Add("MFO For Main Engine");
            lstCol.Add("MFO For Boiler");
            lstCol.Add("MGO Main Engine");
            lstCol.Add("MGO Auxiliary Engine");
            lstCol.Add("MGO Boiler");
            lstCol.Add("Total MFO");
            lstCol.Add("Total MGO");
            lstCol.Add("Bunker Received MFO");
            lstCol.Add("Bunker Received MGO");
            lstCol.Add("LO Main Engine system Sump");
            lstCol.Add("LO Main Engine system Storage Tank");
            lstCol.Add("Attachments");
            return lstCol;
        }

        private static List<string> GetMRColName()
        {
            List<string> lstCol = new List<string>();
            lstCol.Add("Voyage No");
            lstCol.Add("Ship Name");
            lstCol.Add("Report Date");
            lstCol.Add("Call Sign");
            lstCol.Add("Latitude");
            lstCol.Add("Longitude");
            lstCol.Add("Morning Position Time at");
            lstCol.Add("Morning Position Remark");
            lstCol.Add("Vessel Heading");
            lstCol.Add("Course Time");
            lstCol.Add("Course Remark");
            lstCol.Add("Average Speed At Noons");
            lstCol.Add("For Voyage since last full away");
            lstCol.Add("MFO For Main Engine");
            lstCol.Add("MFO For Boiler");
            lstCol.Add("MGO Main Engine");
            lstCol.Add("MGO Auxiliary Engine");
            lstCol.Add("MGO Boiler");
            lstCol.Add("Total MFO");
            lstCol.Add("Total MGO");
            lstCol.Add("Bunker Received MFO");
            lstCol.Add("Bunker Received MGO");
            lstCol.Add("LO Main Engine system Sump");
            lstCol.Add("LO Main Engine system Storage Tank");
            lstCol.Add("Attachments");
            return lstCol;
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;

            try
            {
                string value = cell.CellValue.InnerXml;

                if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
                {
                    return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
                }
                else
                {
                    return value;
                }
            }
            catch { return cell.CellValue.InnerText; }
        }


        public static void addRowExistingMorningExcel(Dictionary<string, dynamic> dictFromData, string destination)
        {
            try
            {
                if(!File.Exists(destination))
                {
                    Export_MorningReport(dictFromData, destination);
                    return;
                }

                using (FileStream fs = new FileStream(destination, FileMode.Open, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    using (SpreadsheetDocument doc = SpreadsheetDocument.Open(fs, true))
                    {
                        WorkbookPart workbookPart = doc.WorkbookPart;
                        //SharedStringTablePart sstpart = workbookPart.GetPartsOfType<SharedStringTablePart>().First();
                        //SharedStringTable sst = sstpart.SharedStringTable;

                        WorksheetPart worksheetPart = workbookPart.WorksheetParts.LastOrDefault();
                        

                        Worksheet sheet = worksheetPart.Worksheet;

                        SheetData sheetData = sheet.GetFirstChild<SheetData>();
                        IEnumerable<Row> rows = sheetData.Descendants<Row>();

                        List<string> receiptField = new List<string>();
                        string ColName = string.Empty;

                        foreach (Cell cell in rows.ElementAt(0))
                        {
                            ColName = GetCellValue(doc, cell);
                            if (ColName.StartsWith(" "))
                                ColName = ColName.Substring(1);
                            receiptField.Add(System.Text.RegularExpressions.Regex.Replace(ColName, @"\t|\n|\r", ""));
                        }

                        if (!receiptField.Contains("Voyage No"))
                        {
                            Export_MorningReport(dictFromData, destination);
                        }
                        else
                            InsertRow(worksheetPart, dictFromData, receiptField);
                    }
                }
            }
            catch (Exception erro)
            {
                throw (new Exception(erro.Message.ToString()));
            }
        }

        public static void InsertRow(WorksheetPart worksheetPart, Dictionary<string, dynamic> dictFromData, List<string> columns)
        {
            SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
            Row lastRow = sheetData.Elements<Row>().LastOrDefault();


            if (lastRow != null)
            {
                DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                foreach (string col in columns)
                {
                    DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                    switch (col)
                    {
                        case "Voyage No":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VoyageNo"]);
                            newRow.AppendChild(cell);
                            break;
                        case "Ship Name":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictFromData["ShipName"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ShipName"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Report Date":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                            cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(dictFromData["ReportDate"]).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                            newRow.AppendChild(cell);
                            break;
                        case "Call Sign":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictFromData["CallSign"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CallSign"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Latitude":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictFromData["Latitude"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Latitude"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                            newRow.AppendChild(cell);
                            break;
                        case "Longitude":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictFromData["Longitude"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Longitude"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                            newRow.AppendChild(cell);
                            break;
                        case "Morning Position Time at":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictFromData["NPTime"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["NPTime"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Morning Position Remark":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                            if (dictFromData.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictFromData["Remarks"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Remarks"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Vessel Heading":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                            if (dictFromData.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictFromData["VesselHeading"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VesselHeading"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Course Time":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("CTime") && !string.IsNullOrEmpty(dictFromData["CTime"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CTime"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Course Remark":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictFromData["Remarks1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["Remarks1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "Average Speed At Noons":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                            if (dictFromData.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictFromData["AverageSpeed_At_Noons"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["AverageSpeed_At_Noons"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "For Voyage since last full away":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictFromData["Forvoyagesincelastfullaway"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Forvoyagesincelastfullaway"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;

                        case "MFO For Main Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictFromData["ForMainEngine"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MFO For Boiler":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                            if (dictFromData.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictFromData["ForBoiler"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Main Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictFromData["ForMainEngine1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Auxiliary Engine":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictFromData["ForAuxiliaryEngines"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForAuxiliaryEngines"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "MGO Boiler":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictFromData["ForBoiler1"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler1"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Total MFO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictFromData["TotalMFO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["TotalMFO"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Total MGO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictFromData["TotalMGO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["TotalMGO"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Bunker Received MFO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("MFO") && !string.IsNullOrEmpty(dictFromData["MFO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MFO"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "Bunker Received MGO":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                            if (dictFromData.ContainsKey("MGO") && !string.IsNullOrEmpty(dictFromData["MGO"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MGO"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "LO Main Engine system Sump":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                            if (dictFromData.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MainEnginesystem_Sump"]));
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                        case "LO Main Engine system Storage Tank":
                            cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                            if (dictFromData.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_StorageTank"]))
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MainEnginesystem_StorageTank"]);
                            else
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                            newRow.AppendChild(cell);
                            break;
                    }
                }
                sheetData.AppendChild(newRow);
            }
            else
            {
                sheetData.InsertAt(new Row() { RowIndex = 0 }, 0);
            }
        }
        public static void Export_MorningReport(Dictionary<string, dynamic> dictFromData, string destination)
        {
            try
            {
                using (var workbook = SpreadsheetDocument.Create(destination, DocumentFormat.OpenXml.SpreadsheetDocumentType.Workbook))
                {
                    var workbookPart = workbook.AddWorkbookPart();

                    workbook.WorkbookPart.Workbook = new DocumentFormat.OpenXml.Spreadsheet.Workbook();

                    workbook.WorkbookPart.Workbook.Sheets = new DocumentFormat.OpenXml.Spreadsheet.Sheets();


                    var sheetPart = workbook.WorkbookPart.AddNewPart<WorksheetPart>();
                    var sheetData = new DocumentFormat.OpenXml.Spreadsheet.SheetData();
                    sheetPart.Worksheet = new DocumentFormat.OpenXml.Spreadsheet.Worksheet(sheetData);

                    DocumentFormat.OpenXml.Spreadsheet.Sheets sheets = workbook.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>();
                    string relationshipId = workbook.WorkbookPart.GetIdOfPart(sheetPart);

                    uint sheetId = 1;
                    if (sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Count() > 0)
                    {
                        sheetId =
                            sheets.Elements<DocumentFormat.OpenXml.Spreadsheet.Sheet>().Select(s => s.SheetId.Value).Max() + 1;
                    }

                    DocumentFormat.OpenXml.Spreadsheet.Sheet sheet = new DocumentFormat.OpenXml.Spreadsheet.Sheet() { Id = relationshipId, SheetId = sheetId, Name = "AR Invoice" };
                    sheets.Append(sheet);

                    DocumentFormat.OpenXml.Spreadsheet.Row headerRow = new DocumentFormat.OpenXml.Spreadsheet.Row();

                    List<String> columns = new List<string>();
                    System.Data.DataTable table = new System.Data.DataTable();
                    List<string> MRColName = GetMRColName();

                    foreach (string column in MRColName)
                    {
                        columns.Add(column);

                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                        cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(column);
                        headerRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(headerRow);

                    DocumentFormat.OpenXml.Spreadsheet.Row newRow = new DocumentFormat.OpenXml.Spreadsheet.Row();
                    foreach (string col in columns)
                    {
                        DocumentFormat.OpenXml.Spreadsheet.Cell cell = new DocumentFormat.OpenXml.Spreadsheet.Cell();
                        switch (col)
                        {
                            case "Voyage No":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VoyageNo"]);
                                newRow.AppendChild(cell);
                                break;
                            case "Ship Name":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictFromData["ShipName"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["ShipName"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Report Date":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Date;
                                cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToDateTime(dictFromData["ReportDate"]).Date.ToString("dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture));
                                newRow.AppendChild(cell);
                                break;
                            case "Call Sign":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictFromData["CallSign"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CallSign"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Latitude":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictFromData["Latitude"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Latitude"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                newRow.AppendChild(cell);
                                break;
                            case "Longitude":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictFromData["Longitude"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Longitude"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();

                                newRow.AppendChild(cell);
                                break;
                            case "Morning Position Time at":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("NPTime") && !string.IsNullOrEmpty(dictFromData["NPTime"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["NPTime"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Morning Position Remark":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                                if (dictFromData.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictFromData["Remarks"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Remarks"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Vessel Heading":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;
                                if (dictFromData.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictFromData["VesselHeading"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["VesselHeading"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "Course Time":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("CTime") && !string.IsNullOrEmpty(dictFromData["CTime"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["CTime"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "Course Remark":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictFromData["Remarks1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["Remarks1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "Average Speed At Noons":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.SharedString;

                                if (dictFromData.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictFromData["AverageSpeed_At_Noons"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["AverageSpeed_At_Noons"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "For Voyage since last full away":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictFromData["Forvoyagesincelastfullaway"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["Forvoyagesincelastfullaway"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;

                            case "MFO For Main Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictFromData["ForMainEngine"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MFO For Boiler":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;

                                if (dictFromData.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictFromData["ForBoiler"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Main Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictFromData["ForMainEngine1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForMainEngine1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Auxiliary Engine":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictFromData["ForAuxiliaryEngines"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForAuxiliaryEngines"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "MGO Boiler":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictFromData["ForBoiler1"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["ForBoiler1"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Total MFO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictFromData["TotalMFO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["TotalMFO"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Total MGO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictFromData["TotalMGO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["TotalMGO"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Bunker Received MFO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("MFO") && !string.IsNullOrEmpty(dictFromData["MFO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MFO"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "Bunker Received MGO":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.String;
                                if (dictFromData.ContainsKey("MGO") && !string.IsNullOrEmpty(dictFromData["MGO"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MGO"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "LO Main Engine system Sump":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                if (dictFromData.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(Convert.ToString(dictFromData["MainEnginesystem_Sump"]));
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                            case "LO Main Engine system Storage Tank":
                                cell.DataType = DocumentFormat.OpenXml.Spreadsheet.CellValues.Number;
                                if (dictFromData.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_StorageTank"]))
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue(dictFromData["MainEnginesystem_StorageTank"]);
                                else
                                    cell.CellValue = new DocumentFormat.OpenXml.Spreadsheet.CellValue();
                                newRow.AppendChild(cell);
                                break;
                        }
                    }
                    sheetData.AppendChild(newRow);

                }
            }
            catch (Exception roor) { }
        }
        #endregion
    }
}

