﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DemoFormSystem.BAL
{
    public class Terminal
    {
        List<TermialTypes> lstType { get; set; }
        public Terminal()
        {
            lstType = new List<TermialTypes>();

            lstType.Add(new TermialTypes { Connection = "Imarsat B", TerminalType = new List<string> { "Generic Imarsat B", "Magna Phone MX2464", "Sailor SP440" } });
            lstType.Add(new TermialTypes { Connection = "Imarsat Mini-M", TerminalType = new List<string> { "Generic Imarsat Mini-M", "Nera Work Phone", "Sailor SP440" } });
            lstType.Add(new TermialTypes { Connection = "Fleet BoardBand", TerminalType = new List<string> { "AddValue Skipper 150", "Furno Felcom 250", "Furno Felcom 250",
                                            "JRC 250", "JRC 500", "JRC withOIU", "satLink250", "Thrane and Thrane sailore 150", "Thrane and Thrane sailore 250", "Thrane and Thrane sailore 500"} });
            lstType.Add(new TermialTypes { Connection = "Iridium", TerminalType = new List<string> { "Iridium 9555", "Iridium Digital", "Eurocom", "Motorola 9555" } });
            lstType.Add(new TermialTypes { Connection = "TCP/IP", TerminalType = new List<string> { "Internet TCP" } });
        }

        public List<string> this[string col]
        {
            get
            {
                foreach (TermialTypes login in lstType)
                {
                    if (col.Equals(login.Connection))
                        return login.TerminalType;
                }
                return new List<string>();
            }
        }

        public static string getIPV4Addresslocalamchine()
        {
            try
            {
                string HostName = Dns.GetHostName();

                IPAddress[] ipaddress = Dns.GetHostAddresses(HostName);

                foreach (IPAddress ip4 in ipaddress.Where(ip => ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork))
                {
                    return ip4.ToString();
                }
                return "";
            }
            catch { return ""; }
        }
    }

    public class TermialTypes
    {
        public string Connection { get; set; }

        public List<string> TerminalType { get; set; }
    }
}
