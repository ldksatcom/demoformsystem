﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoFormSystem
{
    public class Login_DataBase
    {
        public static List<login> lstlogins { get; set; }
        public Login_DataBase() {
            lstlogins = new List<login>();
            lstlogins.Add(new login {EmailID = "karthi3@ldksatcom.in", Password = "smtstestuser@22", CompanyName = "SMTS", VesselName = "India Vessel", Type = "Ship" });
            lstlogins.Add(new login {EmailID = "guru@ldksatcom.in", Password ="smtstestuser@23",CompanyName = "SMTS", VesselName = "India Vessel", Type = "Shore" });
            lstlogins.Add(new login {EmailID = "Shipsmts@ldksatcom.in", Password ="smtstestship", CompanyName = "SMTS", VesselName = "Singapore Vessel", Type = "Ship" });
            lstlogins.Add(new login {EmailID = "Shoresmts@ldksatcom.in", Password ="smtstestshore", CompanyName = "SMTS", VesselName = "Singapore Vessel", Type = "Shore" });
            lstlogins.Add(new login {EmailID = "shippetrovessel1@ldksatcom.in", Password = "petrovessel1ship", CompanyName = "Petrojaya", VesselName = "Petro Sails", Type = "Ship" });
            lstlogins.Add(new login {EmailID = "shorepetro@ldksatcom.in", Password = "petroshore", CompanyName = "Petrojaya", VesselName = "Petro Sails", Type = "Shore" });
        }

        public Tuple<string,string,string,string> this[string col]
        {
            get
            {
                foreach(login login in lstlogins)
                {
                    if (col.Equals(login.EmailID))
                        return new Tuple<string, string, string, string>(login.Password, login.CompanyName, login.VesselName, login.Type);
                }
                return new Tuple<string, string, string, string>(string.Empty, string.Empty, string.Empty, string.Empty);
            }
        }

        public static Dictionary<int,string> GetVesselName(string companyName, List<Dictionary<string, string>> lstcrew)
        {
            Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
            int i = 0;
            keyValuePairs.Add(i, "---Select Vessel---");
            string prevVS = string.Empty;
            foreach (Dictionary<string, string> dictcrew in lstcrew)
            {
                if (dictcrew["company name"].ToString().Equals(companyName))
                {
                   
                        if (!prevVS.Equals(dictcrew["ship name"]))
                        {
                            i++;
                            keyValuePairs.Add(i, dictcrew["ship name"]);
                            prevVS = dictcrew["ship name"];
                        }
                    
                }

                
            }
            //foreach (login login in lstlogins)
            //{
            //    if(login.CompanyName.Equals(companyName))
            //    {
            //        if (!prevVS.Equals(login.VesselName))
            //        {
            //            i++;
            //            keyValuePairs.Add(i, login.VesselName);
            //            prevVS = login.VesselName;
            //        }
            //    }
            //}
            return keyValuePairs;
        }
    }

    public class login
    {
        public string EmailID { get; set; }
        public string Password { get; set; }
        public string CompanyName { get; set; }
        public string VesselName { get; set; }
        public string Type { get; set; }
    }

    public class ContentTypes
    {
        public string Extenstion { get; set; }

        public string ContentType { get; set; }
    }

    public class getContentType
    {
        List<ContentTypes> lstContentTypes = new List<ContentTypes>();

        public getContentType() {
            lstContentTypes.Add(new ContentTypes { Extenstion = ".xls", ContentType= "application/vnd.ms-excel" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".xlsx", ContentType = "application/vnd.ms-excel" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".zip", ContentType = "application/zip" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".3gp", ContentType = "video/3gpp" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".7z", ContentType = "application/x-7z-compressed" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".txt", ContentType = "text/plain" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".tar", ContentType = "application/x-tar" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".rtf", ContentType = "application/rtf" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".rar", ContentType = "application/x-rar-compressed" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ppt", ContentType = "application/vnd.ms-powerpoint" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".pdf", ContentType = "application/pdf" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".png", ContentType = "image/png" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".mpeg", ContentType = "video/mpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".mp3", ContentType = "audio/mpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".js", ContentType = "text/javascript" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".json", ContentType = "application/json" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jpeg", ContentType = "image/jpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jpg", ContentType = "image/jpeg" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".jar", ContentType = "application/java-archive" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ics", ContentType = "text/calendar" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".ico", ContentType = "	image/vnd.microsoft.icon" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".html", ContentType = "text/html" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".gif", ContentType = "image/gif" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".docx", ContentType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".doc", ContentType = "application/msword" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".csv", ContentType = "text/csv" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".css", ContentType = "text/css" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".bmp", ContentType = "image/bmp" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".bin", ContentType = "application/octet-stream" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".avi", ContentType = "video/x-msvideo" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".aac", ContentType = "audio/aac" });
            lstContentTypes.Add(new ContentTypes { Extenstion = ".exe", ContentType = "application/exe" }); 
        }

        public string this[string exten]
        {
            get
            {
                string contType = string.Empty;
                foreach(ContentTypes contentTypes in lstContentTypes)
                {
                    if (contentTypes.Extenstion.Equals(exten))
                        contType = contentTypes.ContentType;
                }
                return contType;
            }
        }
        
    }
}
