﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class gridReports : Form
    {
        public static Dictionary<int, Dictionary<string, dynamic>> dict { get; set; }
        public static Dictionary<string, dynamic> dictSelected { get; set; }
        public gridReports()
        {
            InitializeComponent();
            BindGrid(Petro_NoonReports.isNoon);

            //Add a CheckBox Column to the DataGridView at the first position.
            
            DataGridViewCheckBoxColumn checkBoxColumn = new DataGridViewCheckBoxColumn();
            checkBoxColumn.HeaderText = "";
            checkBoxColumn.Width = 30;
            checkBoxColumn.Name = "checkBoxColumn";
            grid_Reports.Columns.Insert(0, checkBoxColumn);
            grid_Reports.Columns[1].Visible = false;
        }

        private void BindGrid(bool isNoon)
        {
            List<ReportsGrid> lstgrid = new List<ReportsGrid>();
            dict = new Dictionary<int, Dictionary<string, dynamic>>();
            try
            {
                string path = string.Empty;

                if (Petro_NoonReports.isNoon)
                    path = Petro_NoonReports.localDBPath;
                else
                    path = Petro_MorningReports.localDBPath;

                if (!string.IsNullOrEmpty(path))
                {
                    var tuple = Helper.GetValuesfromExcel(path);
                    lstgrid = tuple.Item1;
                    dict = tuple.Item2;
                }
                else
                {
                    if (!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                    {
                        path = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
                        path += "\\ReportData.xlsx";
                    }
                    
                    var tuple = Helper.GetValuesfromExcel(path);
                    lstgrid = tuple.Item1;
                    dict = tuple.Item2;
                }
                #region old
                //using (SqlConnection con = new SqlConnection(ConfigurationSettings.AppSettings["ConnectionString"].ToString()))
                //{
                //    con.Open();

                //    SqlCommand cmd = new SqlCommand();
                //    cmd.Connection = con;
                //    if (isNoon)
                //        cmd.CommandText = string.Format("Select * from DemoNoonReport where ReportType = '{0}'", "Noon");
                //    else
                //        cmd.CommandText = string.Format("Select * from DemoNoonReport where ReportType = '{0}'", "Morning");

                //    SqlDataReader rd = cmd.ExecuteReader();

                //    while (rd.Read())
                //    {
                //        ReportsGrid reportsGrid = new ReportsGrid();
                //        Dictionary<string, dynamic> dicNoonReport = new Dictionary<string, dynamic>();

                //        if (!rd.IsDBNull(rd.GetOrdinal("ID")))
                //        {
                //            reportsGrid.ID = rd.GetInt32(rd.GetOrdinal("ID"));

                //            dicNoonReport.Add("ReportType", "Noon");

                //            dicNoonReport.Add("IsUpdated", "No");
                //        }

                //        if (!rd.IsDBNull(rd.GetOrdinal("VoyageNo")))
                //        {
                //            reportsGrid.VoyageNo = rd.GetString(rd.GetOrdinal("VoyageNo"));
                //            dicNoonReport.Add("VoyageNo", reportsGrid.VoyageNo);
                //        }

                //        if (!rd.IsDBNull(rd.GetOrdinal("ShipName")))
                //        {
                //            reportsGrid.ShipName = rd.GetString(rd.GetOrdinal("ShipName"));
                //            dicNoonReport.Add("ShipName", reportsGrid.ShipName);
                //        }

                //        if (!rd.IsDBNull(rd.GetOrdinal("ReportDate")))
                //        {
                //            reportsGrid.ReportDate = rd.GetDateTime(rd.GetOrdinal("ReportDate"));
                //            dicNoonReport.Add("ReportDate", Convert.ToDateTime(reportsGrid.ReportDate).ToString("dd/MM/yyyy"));
                //        }

                //        if (!rd.IsDBNull(rd.GetOrdinal("CallSign")))
                //        {
                //            reportsGrid.CallSign = rd.GetString(rd.GetOrdinal("CallSign"));
                //            dicNoonReport.Add("CallSign", reportsGrid.CallSign);
                //        }

                //        if(!rd.IsDBNull(rd.GetOrdinal("Latitude")))
                //            dicNoonReport.Add("Latitude", rd.GetString(rd.GetOrdinal("Latitude")));

                //        if(!rd.IsDBNull(rd.GetOrdinal("Longitude")))
                //            dicNoonReport.Add("Longitude", rd.GetString(rd.GetOrdinal("Longitude")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("NPTime")))
                //        {
                //            reportsGrid.PositionTime = rd.GetTimeSpan(rd.GetOrdinal("NPTime"));
                //            dicNoonReport.Add("NPTime", reportsGrid.PositionTime);
                //        }

                //        if(!rd.IsDBNull(rd.GetOrdinal("Remarks")))
                //            dicNoonReport.Add("Remarks", rd.GetString(rd.GetOrdinal("Remarks")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("VesselHeading")))
                //            dicNoonReport.Add("VesselHeading", rd.GetString(rd.GetOrdinal("VesselHeading")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("CTime")))
                //            dicNoonReport.Add("CTime", rd.GetTimeSpan(rd.GetOrdinal("CTime")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("Remarks1")))
                //            dicNoonReport.Add("Remarks1", rd.GetString(rd.GetOrdinal("Remarks1")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("AverageSpeed_At_Noons")))
                //            dicNoonReport.Add("AverageSpeed_At_Noons", rd.GetString(rd.GetOrdinal("AverageSpeed_At_Noons")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("Forvoyagesincelastfullaway")))
                //            dicNoonReport.Add("Forvoyagesincelastfullaway", rd.GetString(rd.GetOrdinal("Forvoyagesincelastfullaway")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MFOForMainEngine")))
                //            dicNoonReport.Add("ForMainEngine", rd.GetString(rd.GetOrdinal("MFOForMainEngine")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MFOForBoiler")))
                //            dicNoonReport.Add("ForBoiler", rd.GetString(rd.GetOrdinal("MFOForBoiler")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MGOForMainEngine")))
                //            dicNoonReport.Add("ForMainEngine1", rd.GetString(rd.GetOrdinal("MGOForMainEngine")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MGOForAuxiliaryEngines")))
                //            dicNoonReport.Add("ForAuxiliaryEngines", rd.GetString(rd.GetOrdinal("MGOForAuxiliaryEngines")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MGOForBoiler")))
                //            dicNoonReport.Add("ForBoiler1", rd.GetString(rd.GetOrdinal("MGOForBoiler")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("TotalMFO")))
                //            dicNoonReport.Add("TotalMFO", rd.GetString(rd.GetOrdinal("TotalMFO")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("TotalMGO")))
                //            dicNoonReport.Add("TotalMGO", rd.GetString(rd.GetOrdinal("TotalMGO")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MFO")))
                //            dicNoonReport.Add("MFO", rd.GetString(rd.GetOrdinal("MFO")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("MGO")))
                //            dicNoonReport.Add("MGO", rd.GetString(rd.GetOrdinal("MGO")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("LOMainEnginesystem_Sump")))
                //            dicNoonReport.Add("MainEnginesystem_Sump", rd.GetDecimal(rd.GetOrdinal("LOMainEnginesystem_Sump")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("LOMainEnginesystem_StorageTank")))
                //            dicNoonReport.Add("MainEnginesystem_StorageTank", rd.GetDecimal(rd.GetOrdinal("LOMainEnginesystem_StorageTank")));

                //        if (!rd.IsDBNull(rd.GetOrdinal("isUpdated")))
                //            dicNoonReport.Add("isUpdated", rd.GetString(rd.GetOrdinal("isUpdated")));


                //        dict.Add(reportsGrid.ID, dicNoonReport);

                //        lstgrid.Add(reportsGrid);
                //    }
                //}
                #endregion
            }
            catch (Exception rr)
            {
            }

            grid_Reports.DataSource = lstgrid;
            
        }

        private void btnSelected_Click(object sender, EventArgs e)
        {
            string message = string.Empty;
            foreach (DataGridViewRow row in grid_Reports.Rows)
            {
                bool isSelected = Convert.ToBoolean(row.Cells["checkBoxColumn"].Value);
                if (isSelected)
                {
                    int id = Convert.ToInt32(row.Cells["ID"].Value);
                    try
                    {
                        dictSelected = dict[id];
                    }
                    catch (Exception ee) {
                        MessageBox.Show(string.Format("Please select the {0} row data only.", Petro_NoonReports.isNoon? "Noon Report": "Morning Report"));
                        return;
                    }
                    if (Petro_NoonReports.isNoon)
                    {
                        this.Close();
                        Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
                        petro_NoonReports.BindForm(dictSelected, true);
                        petro_NoonReports.Show();
                    }
                    else
                    {
                        this.Close();
                        Petro_MorningReports petro_MorningReports = new Petro_MorningReports();
                        petro_MorningReports.BindForm(dictSelected, true);
                        petro_MorningReports.Show();
                    }

                    break;
                    //message += Environment.NewLine;
                    //message += row.Cells["ID"].Value.ToString();
                }
            }
            //MessageBox.Show("Selected Values" + message);
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            if (Petro_NoonReports.isNoon)
            {
                this.Close();
                Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
                petro_NoonReports.Show();
            }
            else
            {
                this.Close();
                Petro_MorningReports petro_MorningReports = new Petro_MorningReports();
                petro_MorningReports.Show();
            }
        }
    }

    public class ReportsGrid
    {
        public int ID { get; set; }
        public string VoyageNo { get; set; }
        public string ShipName { get; set; }
        public DateTime ReportDate { get; set; }
        public string CallSign { get; set; }
        public TimeSpan PositionTime { get; set; }
    }
}
