﻿namespace DemoFormSystem
{
    partial class FormNames
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_FormClose = new System.Windows.Forms.Button();
            this.Display_formcombo = new System.Windows.Forms.ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(798, 445);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.panel3.Controls.Add(this.btn_FormClose);
            this.panel3.Controls.Add(this.Display_formcombo);
            this.panel3.Location = new System.Drawing.Point(3, 94);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(792, 341);
            this.panel3.TabIndex = 1;
            // 
            // btn_FormClose
            // 
            this.btn_FormClose.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_FormClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_FormClose.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_FormClose.Location = new System.Drawing.Point(714, 306);
            this.btn_FormClose.Name = "btn_FormClose";
            this.btn_FormClose.Size = new System.Drawing.Size(75, 32);
            this.btn_FormClose.TabIndex = 1;
            this.btn_FormClose.Text = "Close";
            this.btn_FormClose.UseVisualStyleBackColor = false;
            this.btn_FormClose.Click += new System.EventHandler(this.btn_FormClose_Click);
            // 
            // Display_formcombo
            // 
            this.Display_formcombo.FormattingEnabled = true;
            this.Display_formcombo.Location = new System.Drawing.Point(242, 77);
            this.Display_formcombo.Name = "Display_formcombo";
            this.Display_formcombo.Size = new System.Drawing.Size(234, 21);
            this.Display_formcombo.TabIndex = 0;
            this.Display_formcombo.Text = "--Select--";
            this.Display_formcombo.SelectedIndexChanged += new System.EventHandler(this.Display_formcombo_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.lblCompanyName);
            this.panel2.Location = new System.Drawing.Point(3, 9);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(785, 79);
            this.panel2.TabIndex = 0;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblCompanyName.Location = new System.Drawing.Point(273, 25);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(168, 31);
            this.lblCompanyName.TabIndex = 0;
            this.lblCompanyName.Text = "Form System";
            // 
            // FormNames
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "FormNames";
            this.Text = "NewDashboard";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.ComboBox Display_formcombo;
        private System.Windows.Forms.Button btn_FormClose;
    }
}