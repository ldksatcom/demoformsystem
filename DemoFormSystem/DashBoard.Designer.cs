﻿namespace DemoFormSystem
{
    partial class DashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_Home = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.pnlNoon = new System.Windows.Forms.Panel();
            this.lblNoonDashboard = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pnlMorning = new System.Windows.Forms.Panel();
            this.lblMorningDashBoard = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlNoon.SuspendLayout();
            this.pnlMorning.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(2, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(799, 446);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btn_Home);
            this.panel3.Controls.Add(this.btnClose);
            this.panel3.Controls.Add(this.pnlNoon);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.pnlMorning);
            this.panel3.Location = new System.Drawing.Point(3, 60);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(793, 374);
            this.panel3.TabIndex = 2;
            // 
            // btn_Home
            // 
            this.btn_Home.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_Home.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Home.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_Home.Location = new System.Drawing.Point(0, 343);
            this.btn_Home.Name = "btn_Home";
            this.btn_Home.Size = new System.Drawing.Size(75, 28);
            this.btn_Home.TabIndex = 114;
            this.btn_Home.Text = "Home";
            this.btn_Home.UseVisualStyleBackColor = false;
            this.btn_Home.Click += new System.EventHandler(this.btn_Home_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.Window;
            this.btnClose.Location = new System.Drawing.Point(715, 343);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 28);
            this.btnClose.TabIndex = 112;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pnlNoon
            // 
            this.pnlNoon.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlNoon.Controls.Add(this.lblNoonDashboard);
            this.pnlNoon.Location = new System.Drawing.Point(452, 63);
            this.pnlNoon.Name = "pnlNoon";
            this.pnlNoon.Size = new System.Drawing.Size(222, 100);
            this.pnlNoon.TabIndex = 2;
            this.pnlNoon.Click += new System.EventHandler(this.pnlNoon_Click);
            this.pnlNoon.MouseLeave += new System.EventHandler(this.pnlNoon_MouseLeave);
            this.pnlNoon.MouseHover += new System.EventHandler(this.pnlNoon_MouseHover);
            // 
            // lblNoonDashboard
            // 
            this.lblNoonDashboard.AutoSize = true;
            this.lblNoonDashboard.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoonDashboard.Location = new System.Drawing.Point(3, 34);
            this.lblNoonDashboard.Name = "lblNoonDashboard";
            this.lblNoonDashboard.Size = new System.Drawing.Size(172, 31);
            this.lblNoonDashboard.TabIndex = 0;
            this.lblNoonDashboard.Text = "Noon Reports";
            this.lblNoonDashboard.Click += new System.EventHandler(this.lblNoonDashboard_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(7, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(191, 24);
            this.label2.TabIndex = 1;
            this.label2.Text = "Reports Dash Board";
            // 
            // pnlMorning
            // 
            this.pnlMorning.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlMorning.Controls.Add(this.lblMorningDashBoard);
            this.pnlMorning.Location = new System.Drawing.Point(92, 63);
            this.pnlMorning.Name = "pnlMorning";
            this.pnlMorning.Size = new System.Drawing.Size(230, 100);
            this.pnlMorning.TabIndex = 0;
            this.pnlMorning.Click += new System.EventHandler(this.pnlMorning_Click);
            this.pnlMorning.MouseLeave += new System.EventHandler(this.pnlMorning_MouseLeave);
            this.pnlMorning.MouseHover += new System.EventHandler(this.pnlMorning_MouseHover);
            // 
            // lblMorningDashBoard
            // 
            this.lblMorningDashBoard.AutoSize = true;
            this.lblMorningDashBoard.Font = new System.Drawing.Font("Times New Roman", 20.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMorningDashBoard.Location = new System.Drawing.Point(3, 34);
            this.lblMorningDashBoard.Name = "lblMorningDashBoard";
            this.lblMorningDashBoard.Size = new System.Drawing.Size(210, 31);
            this.lblMorningDashBoard.TabIndex = 0;
            this.lblMorningDashBoard.Text = "Morning Reports";
            this.lblMorningDashBoard.Click += new System.EventHandler(this.lblMorningDashBoard_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.lblCompanyName);
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(799, 54);
            this.panel2.TabIndex = 1;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCompanyName.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblCompanyName.Location = new System.Drawing.Point(187, 7);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(378, 31);
            this.lblCompanyName.TabIndex = 0;
            this.lblCompanyName.Text = "Petrojaya Report Form System";
            // 
            // DashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel1);
            this.Name = "DashBoard";
            this.Text = "DashBoard";
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlNoon.ResumeLayout(false);
            this.pnlNoon.PerformLayout();
            this.pnlMorning.ResumeLayout(false);
            this.pnlMorning.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Windows.Forms.Panel pnlNoon;
        private System.Windows.Forms.Label lblNoonDashboard;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlMorning;
        private System.Windows.Forms.Label lblMorningDashBoard;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btn_Home;
    }
}