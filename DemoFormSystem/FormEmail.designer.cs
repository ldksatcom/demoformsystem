﻿namespace Petrojayaforminsert
{
    partial class EmailForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddBlockList = new System.Windows.Forms.Button();
            this.txtBlockList = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.pnlBorder = new System.Windows.Forms.Panel();
            this.lblConnectionType = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblIsColsed = new System.Windows.Forms.Label();
            this.lblIsDisconnect = new System.Windows.Forms.Label();
            this.lblIsTransfer = new System.Windows.Forms.Label();
            this.lblIsLog = new System.Windows.Forms.Label();
            this.lblIsConnecting = new System.Windows.Forms.Label();
            this.lblIsOpening = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblIsFormSave = new System.Windows.Forms.Label();
            this.lblIsSave = new System.Windows.Forms.Label();
            this.lblIsCompress = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.MessageSize_Combo = new System.Windows.Forms.ComboBox();
            this.lbl_MessageSize = new System.Windows.Forms.Label();
            this.lbl_attachmentsize = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.AttachmentSize_ComboBox = new System.Windows.Forms.ComboBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lnkEditForm = new System.Windows.Forms.LinkLabel();
            this.lnkCancel = new System.Windows.Forms.LinkLabel();
            this.btnClear = new System.Windows.Forms.LinkLabel();
            this.lnkDownload = new System.Windows.Forms.LinkLabel();
            this.btnSend = new System.Windows.Forms.Button();
            this.Remove_LinkLabel = new System.Windows.Forms.LinkLabel();
            this.btn_browse = new System.Windows.Forms.Button();
            this.txt_OtherAttachments = new System.Windows.Forms.Label();
            this.lblEmailAttach = new System.Windows.Forms.Label();
            this.lblCompressedSize = new System.Windows.Forms.Label();
            this.lstboxResult = new System.Windows.Forms.ListBox();
            this.prgrsCompressed = new System.Windows.Forms.ProgressBar();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSubject = new System.Windows.Forms.TextBox();
            this.lblSubject = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtToEmailid = new System.Windows.Forms.TextBox();
            this.lblToEmail = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtCCMails = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnlBorder.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAddBlockList);
            this.panel1.Controls.Add(this.txtBlockList);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Location = new System.Drawing.Point(0, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(743, 373);
            this.panel1.TabIndex = 0;
            // 
            // btnAddBlockList
            // 
            this.btnAddBlockList.AllowDrop = true;
            this.btnAddBlockList.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnAddBlockList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddBlockList.ForeColor = System.Drawing.SystemColors.Window;
            this.btnAddBlockList.Location = new System.Drawing.Point(533, 346);
            this.btnAddBlockList.Name = "btnAddBlockList";
            this.btnAddBlockList.Size = new System.Drawing.Size(75, 23);
            this.btnAddBlockList.TabIndex = 34;
            this.btnAddBlockList.Text = "Add";
            this.btnAddBlockList.UseVisualStyleBackColor = false;
            this.btnAddBlockList.Click += new System.EventHandler(this.btnAddBlockList_Click);
            // 
            // txtBlockList
            // 
            this.txtBlockList.Location = new System.Drawing.Point(110, 348);
            this.txtBlockList.Name = "txtBlockList";
            this.txtBlockList.Size = new System.Drawing.Size(421, 20);
            this.txtBlockList.TabIndex = 33;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 348);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(51, 13);
            this.label17.TabIndex = 32;
            this.label17.Text = "WhiteList";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pnlBorder);
            this.panel3.Controls.Add(this.MessageSize_Combo);
            this.panel3.Controls.Add(this.lbl_MessageSize);
            this.panel3.Controls.Add(this.lbl_attachmentsize);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.AttachmentSize_ComboBox);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Controls.Add(this.Remove_LinkLabel);
            this.panel3.Controls.Add(this.btn_browse);
            this.panel3.Controls.Add(this.txt_OtherAttachments);
            this.panel3.Controls.Add(this.lblEmailAttach);
            this.panel3.Controls.Add(this.lblCompressedSize);
            this.panel3.Controls.Add(this.lstboxResult);
            this.panel3.Controls.Add(this.prgrsCompressed);
            this.panel3.Controls.Add(this.tableLayoutPanel2);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.tableLayoutPanel1);
            this.panel3.Location = new System.Drawing.Point(3, 49);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(737, 296);
            this.panel3.TabIndex = 12;
            // 
            // pnlBorder
            // 
            this.pnlBorder.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlBorder.Controls.Add(this.lblConnectionType);
            this.pnlBorder.Controls.Add(this.tableLayoutPanel5);
            this.pnlBorder.Controls.Add(this.label9);
            this.pnlBorder.Controls.Add(this.label2);
            this.pnlBorder.Controls.Add(this.label12);
            this.pnlBorder.Controls.Add(this.tableLayoutPanel4);
            this.pnlBorder.Location = new System.Drawing.Point(612, 3);
            this.pnlBorder.Name = "pnlBorder";
            this.pnlBorder.Size = new System.Drawing.Size(125, 289);
            this.pnlBorder.TabIndex = 31;
            // 
            // lblConnectionType
            // 
            this.lblConnectionType.AutoSize = true;
            this.lblConnectionType.Location = new System.Drawing.Point(32, 261);
            this.lblConnectionType.Name = "lblConnectionType";
            this.lblConnectionType.Size = new System.Drawing.Size(10, 13);
            this.lblConnectionType.TabIndex = 38;
            this.lblConnectionType.Text = ".";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.79661F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.20339F));
            this.tableLayoutPanel5.Controls.Add(this.lblIsColsed, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.lblIsDisconnect, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.lblIsTransfer, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblIsLog, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.lblIsConnecting, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblIsOpening, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label16, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.label14, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.label13, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label11, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label15, 1, 4);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 108);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 6;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(118, 117);
            this.tableLayoutPanel5.TabIndex = 34;
            // 
            // lblIsColsed
            // 
            this.lblIsColsed.AutoSize = true;
            this.lblIsColsed.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsColsed.ForeColor = System.Drawing.Color.Red;
            this.lblIsColsed.Location = new System.Drawing.Point(3, 96);
            this.lblIsColsed.Name = "lblIsColsed";
            this.lblIsColsed.Size = new System.Drawing.Size(14, 19);
            this.lblIsColsed.TabIndex = 45;
            this.lblIsColsed.Text = "X";
            // 
            // lblIsDisconnect
            // 
            this.lblIsDisconnect.AutoSize = true;
            this.lblIsDisconnect.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsDisconnect.ForeColor = System.Drawing.Color.Red;
            this.lblIsDisconnect.Location = new System.Drawing.Point(3, 76);
            this.lblIsDisconnect.Name = "lblIsDisconnect";
            this.lblIsDisconnect.Size = new System.Drawing.Size(14, 19);
            this.lblIsDisconnect.TabIndex = 44;
            this.lblIsDisconnect.Text = "X";
            // 
            // lblIsTransfer
            // 
            this.lblIsTransfer.AutoSize = true;
            this.lblIsTransfer.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsTransfer.ForeColor = System.Drawing.Color.Red;
            this.lblIsTransfer.Location = new System.Drawing.Point(3, 56);
            this.lblIsTransfer.Name = "lblIsTransfer";
            this.lblIsTransfer.Size = new System.Drawing.Size(14, 19);
            this.lblIsTransfer.TabIndex = 43;
            this.lblIsTransfer.Text = "X";
            // 
            // lblIsLog
            // 
            this.lblIsLog.AutoSize = true;
            this.lblIsLog.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsLog.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsLog.Location = new System.Drawing.Point(3, 36);
            this.lblIsLog.Name = "lblIsLog";
            this.lblIsLog.Size = new System.Drawing.Size(14, 19);
            this.lblIsLog.TabIndex = 42;
            this.lblIsLog.Text = "✓";
            // 
            // lblIsConnecting
            // 
            this.lblIsConnecting.AutoSize = true;
            this.lblIsConnecting.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsConnecting.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsConnecting.Location = new System.Drawing.Point(3, 18);
            this.lblIsConnecting.Name = "lblIsConnecting";
            this.lblIsConnecting.Size = new System.Drawing.Size(14, 18);
            this.lblIsConnecting.TabIndex = 41;
            this.lblIsConnecting.Text = "✓";
            // 
            // lblIsOpening
            // 
            this.lblIsOpening.AutoSize = true;
            this.lblIsOpening.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsOpening.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsOpening.Location = new System.Drawing.Point(3, 0);
            this.lblIsOpening.Name = "lblIsOpening";
            this.lblIsOpening.Size = new System.Drawing.Size(14, 18);
            this.lblIsOpening.TabIndex = 40;
            this.lblIsOpening.Text = "✓";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(23, 96);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Closing";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(23, 56);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Transferring";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 36);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(60, 13);
            this.label13.TabIndex = 35;
            this.label13.Text = "Logging on";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(47, 13);
            this.label10.TabIndex = 35;
            this.label10.Text = "Opening";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(23, 18);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Connecting";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(23, 76);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 35;
            this.label15.Text = "Disconnecting";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(-1, 92);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(66, 13);
            this.label9.TabIndex = 33;
            this.label9.Text = "Connections";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Preparation";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1, 238);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 13);
            this.label12.TabIndex = 37;
            this.label12.Text = "Connection Type";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.79661F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 82.20339F));
            this.tableLayoutPanel4.Controls.Add(this.lblIsFormSave, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.lblIsSave, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblIsCompress, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label7, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 47.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 52.5F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(118, 54);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // lblIsFormSave
            // 
            this.lblIsFormSave.AutoSize = true;
            this.lblIsFormSave.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsFormSave.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsFormSave.Location = new System.Drawing.Point(3, 34);
            this.lblIsFormSave.Name = "lblIsFormSave";
            this.lblIsFormSave.Size = new System.Drawing.Size(14, 19);
            this.lblIsFormSave.TabIndex = 41;
            this.lblIsFormSave.Text = "✓";
            // 
            // lblIsSave
            // 
            this.lblIsSave.AutoSize = true;
            this.lblIsSave.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsSave.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsSave.Location = new System.Drawing.Point(3, 16);
            this.lblIsSave.Name = "lblIsSave";
            this.lblIsSave.Size = new System.Drawing.Size(14, 18);
            this.lblIsSave.TabIndex = 40;
            this.lblIsSave.Text = "✓";
            // 
            // lblIsCompress
            // 
            this.lblIsCompress.AutoSize = true;
            this.lblIsCompress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIsCompress.ForeColor = System.Drawing.Color.ForestGreen;
            this.lblIsCompress.Location = new System.Drawing.Point(3, 0);
            this.lblIsCompress.Name = "lblIsCompress";
            this.lblIsCompress.Size = new System.Drawing.Size(14, 16);
            this.lblIsCompress.TabIndex = 39;
            this.lblIsCompress.Text = "✓";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 33;
            this.label8.Text = "Form Save";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(23, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(75, 13);
            this.label7.TabIndex = 33;
            this.label7.Text = "Save local DB";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(23, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 13);
            this.label4.TabIndex = 33;
            this.label4.Text = "Compressing";
            // 
            // MessageSize_Combo
            // 
            this.MessageSize_Combo.FormattingEnabled = true;
            this.MessageSize_Combo.Location = new System.Drawing.Point(488, 271);
            this.MessageSize_Combo.Name = "MessageSize_Combo";
            this.MessageSize_Combo.Size = new System.Drawing.Size(121, 21);
            this.MessageSize_Combo.TabIndex = 22;
            this.MessageSize_Combo.Text = "--Select--";
            // 
            // lbl_MessageSize
            // 
            this.lbl_MessageSize.AutoSize = true;
            this.lbl_MessageSize.Location = new System.Drawing.Point(348, 274);
            this.lbl_MessageSize.Name = "lbl_MessageSize";
            this.lbl_MessageSize.Size = new System.Drawing.Size(73, 13);
            this.lbl_MessageSize.TabIndex = 21;
            this.lbl_MessageSize.Text = "Message Size";
            // 
            // lbl_attachmentsize
            // 
            this.lbl_attachmentsize.AutoSize = true;
            this.lbl_attachmentsize.Location = new System.Drawing.Point(6, 274);
            this.lbl_attachmentsize.Name = "lbl_attachmentsize";
            this.lbl_attachmentsize.Size = new System.Drawing.Size(84, 13);
            this.lbl_attachmentsize.TabIndex = 19;
            this.lbl_attachmentsize.Text = "Attachment Size";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 112);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 13);
            this.label1.TabIndex = 30;
            this.label1.Text = "Attachments";
            // 
            // AttachmentSize_ComboBox
            // 
            this.AttachmentSize_ComboBox.FormattingEnabled = true;
            this.AttachmentSize_ComboBox.Location = new System.Drawing.Point(107, 271);
            this.AttachmentSize_ComboBox.Name = "AttachmentSize_ComboBox";
            this.AttachmentSize_ComboBox.Size = new System.Drawing.Size(121, 21);
            this.AttachmentSize_ComboBox.TabIndex = 20;
            this.AttachmentSize_ComboBox.Text = "--Select--";
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel3);
            this.panel4.Controls.Add(this.btnSend);
            this.panel4.Location = new System.Drawing.Point(6, 224);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(602, 44);
            this.panel4.TabIndex = 29;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.930403F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.49635F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.46715F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.57664F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.lnkEditForm, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lnkCancel, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.btnClear, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.lnkDownload, 1, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(207, 23);
            this.tableLayoutPanel3.TabIndex = 23;
            // 
            // lnkEditForm
            // 
            this.lnkEditForm.Location = new System.Drawing.Point(3, 0);
            this.lnkEditForm.Name = "lnkEditForm";
            this.lnkEditForm.Size = new System.Drawing.Size(1, 23);
            this.lnkEditForm.TabIndex = 0;
            // 
            // lnkCancel
            // 
            this.lnkCancel.AutoSize = true;
            this.lnkCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkCancel.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnkCancel.Location = new System.Drawing.Point(140, 0);
            this.lnkCancel.Name = "lnkCancel";
            this.lnkCancel.Size = new System.Drawing.Size(49, 17);
            this.lnkCancel.TabIndex = 19;
            this.lnkCancel.TabStop = true;
            this.lnkCancel.Text = "cancel";
            this.lnkCancel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkCancel_LinkClicked);
            // 
            // btnClear
            // 
            this.btnClear.AutoSize = true;
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.btnClear.Location = new System.Drawing.Point(82, 0);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(39, 17);
            this.btnClear.TabIndex = 10;
            this.btnClear.TabStop = true;
            this.btnClear.Text = "clear";
            this.btnClear.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnClear_LinkClicked_1);
            // 
            // lnkDownload
            // 
            this.lnkDownload.AutoSize = true;
            this.lnkDownload.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lnkDownload.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.lnkDownload.Location = new System.Drawing.Point(8, 0);
            this.lnkDownload.Name = "lnkDownload";
            this.lnkDownload.Size = new System.Drawing.Size(68, 17);
            this.lnkDownload.TabIndex = 20;
            this.lnkDownload.TabStop = true;
            this.lnkDownload.Text = "download";
            this.lnkDownload.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkDownload_LinkClicked);
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSend.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSend.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSend.Location = new System.Drawing.Point(524, 0);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(75, 41);
            this.btnSend.TabIndex = 7;
            this.btnSend.Text = "Send";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click_1);
            // 
            // Remove_LinkLabel
            // 
            this.Remove_LinkLabel.AutoSize = true;
            this.Remove_LinkLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Remove_LinkLabel.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.Remove_LinkLabel.LinkColor = System.Drawing.Color.Red;
            this.Remove_LinkLabel.Location = new System.Drawing.Point(507, 116);
            this.Remove_LinkLabel.Name = "Remove_LinkLabel";
            this.Remove_LinkLabel.Size = new System.Drawing.Size(21, 20);
            this.Remove_LinkLabel.TabIndex = 28;
            this.Remove_LinkLabel.TabStop = true;
            this.Remove_LinkLabel.Text = "X";
            this.Remove_LinkLabel.Visible = false;
            this.Remove_LinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Remove_LinkLabel_LinkClicked);
            // 
            // btn_browse
            // 
            this.btn_browse.AllowDrop = true;
            this.btn_browse.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btn_browse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_browse.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_browse.Location = new System.Drawing.Point(534, 115);
            this.btn_browse.Name = "btn_browse";
            this.btn_browse.Size = new System.Drawing.Size(75, 23);
            this.btn_browse.TabIndex = 25;
            this.btn_browse.Text = "Browse";
            this.btn_browse.UseVisualStyleBackColor = false;
            this.btn_browse.Click += new System.EventHandler(this.btn_browse_Click);
            // 
            // txt_OtherAttachments
            // 
            this.txt_OtherAttachments.AutoSize = true;
            this.txt_OtherAttachments.Location = new System.Drawing.Point(159, 112);
            this.txt_OtherAttachments.Name = "txt_OtherAttachments";
            this.txt_OtherAttachments.Size = new System.Drawing.Size(0, 13);
            this.txt_OtherAttachments.TabIndex = 27;
            // 
            // lblEmailAttach
            // 
            this.lblEmailAttach.AutoSize = true;
            this.lblEmailAttach.Location = new System.Drawing.Point(539, 255);
            this.lblEmailAttach.Name = "lblEmailAttach";
            this.lblEmailAttach.Size = new System.Drawing.Size(0, 13);
            this.lblEmailAttach.TabIndex = 15;
            this.lblEmailAttach.Visible = false;
            // 
            // lblCompressedSize
            // 
            this.lblCompressedSize.AutoSize = true;
            this.lblCompressedSize.Location = new System.Drawing.Point(240, 173);
            this.lblCompressedSize.Name = "lblCompressedSize";
            this.lblCompressedSize.Size = new System.Drawing.Size(0, 13);
            this.lblCompressedSize.TabIndex = 14;
            // 
            // lstboxResult
            // 
            this.lstboxResult.FormattingEnabled = true;
            this.lstboxResult.Location = new System.Drawing.Point(7, 168);
            this.lstboxResult.Name = "lstboxResult";
            this.lstboxResult.Size = new System.Drawing.Size(602, 56);
            this.lstboxResult.TabIndex = 13;
            // 
            // prgrsCompressed
            // 
            this.prgrsCompressed.Location = new System.Drawing.Point(3, 144);
            this.prgrsCompressed.Name = "prgrsCompressed";
            this.prgrsCompressed.Size = new System.Drawing.Size(225, 23);
            this.prgrsCompressed.TabIndex = 12;
            this.prgrsCompressed.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.95868F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.04132F));
            this.tableLayoutPanel2.Controls.Add(this.txtSubject, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblSubject, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(7, 81);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(602, 28);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // txtSubject
            // 
            this.txtSubject.Location = new System.Drawing.Point(153, 3);
            this.txtSubject.Name = "txtSubject";
            this.txtSubject.Size = new System.Drawing.Size(446, 20);
            this.txtSubject.TabIndex = 6;
            // 
            // lblSubject
            // 
            this.lblSubject.AutoSize = true;
            this.lblSubject.Location = new System.Drawing.Point(3, 0);
            this.lblSubject.Name = "lblSubject";
            this.lblSubject.Size = new System.Drawing.Size(43, 13);
            this.lblSubject.TabIndex = 0;
            this.lblSubject.Text = "Subject";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.label6.Location = new System.Drawing.Point(3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 20);
            this.label6.TabIndex = 4;
            this.label6.Text = "New Email";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.12397F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.87603F));
            this.tableLayoutPanel1.Controls.Add(this.txtToEmailid, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblToEmail, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCCMails, 1, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 28);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(602, 51);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // txtToEmailid
            // 
            this.txtToEmailid.Location = new System.Drawing.Point(154, 3);
            this.txtToEmailid.Name = "txtToEmailid";
            this.txtToEmailid.Size = new System.Drawing.Size(445, 20);
            this.txtToEmailid.TabIndex = 5;
            // 
            // lblToEmail
            // 
            this.lblToEmail.AutoSize = true;
            this.lblToEmail.Location = new System.Drawing.Point(3, 0);
            this.lblToEmail.Name = "lblToEmail";
            this.lblToEmail.Size = new System.Drawing.Size(20, 13);
            this.lblToEmail.TabIndex = 4;
            this.lblToEmail.Text = "To";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 24);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(21, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "CC";
            // 
            // txtCCMails
            // 
            this.txtCCMails.Location = new System.Drawing.Point(154, 27);
            this.txtCCMails.Name = "txtCCMails";
            this.txtCCMails.Size = new System.Drawing.Size(445, 20);
            this.txtCCMails.TabIndex = 7;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(0, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(740, 42);
            this.panel2.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 33);
            this.label3.TabIndex = 12;
            this.label3.Text = "SMTS Pte ";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // EmailForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(744, 375);
            this.Controls.Add(this.panel1);
            this.Name = "EmailForm";
            this.Text = "FormEmail";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnlBorder.ResumeLayout(false);
            this.pnlBorder.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblEmailAttach;
        private System.Windows.Forms.Label lblCompressedSize;
        private System.Windows.Forms.ListBox lstboxResult;
        private System.Windows.Forms.ProgressBar prgrsCompressed;
        private System.Windows.Forms.LinkLabel btnClear;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label lblSubject;
        private System.Windows.Forms.TextBox txtSubject;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox txtToEmailid;
        private System.Windows.Forms.Label lblToEmail;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtCCMails;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbl_attachmentsize;
        private System.Windows.Forms.ComboBox AttachmentSize_ComboBox;
        private System.Windows.Forms.Label lbl_MessageSize;
        private System.Windows.Forms.ComboBox MessageSize_Combo;
        private System.Windows.Forms.Button btn_browse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label txt_OtherAttachments;
        private System.Windows.Forms.LinkLabel Remove_LinkLabel;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.LinkLabel lnkCancel;
        private System.Windows.Forms.LinkLabel lnkDownload;
        private System.Windows.Forms.LinkLabel lnkEditForm;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlBorder;
        private System.Windows.Forms.Label lblConnectionType;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label lblIsColsed;
        private System.Windows.Forms.Label lblIsDisconnect;
        private System.Windows.Forms.Label lblIsTransfer;
        private System.Windows.Forms.Label lblIsLog;
        private System.Windows.Forms.Label lblIsConnecting;
        private System.Windows.Forms.Label lblIsOpening;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label lblIsFormSave;
        private System.Windows.Forms.Label lblIsSave;
        private System.Windows.Forms.Label lblIsCompress;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAddBlockList;
        private System.Windows.Forms.TextBox txtBlockList;
        private System.Windows.Forms.Label label17;
    }
}