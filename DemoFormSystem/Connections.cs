﻿using Compressed_Library;
using Petrojayaforminsert;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class Connections : Form
    {

        public string Email { get; set; }

        public string Password { get; set; }

        public string ToEmail { get; set; }

        public string attachFileName { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string ccMail { get; set; }

        public string OtherAttac { get; set; }

        public FileInfo fileinfo { get; set; }

        public static bool isConnection { get; set; }

        public static bool isOpening { get; set; }

        public static bool isLogging { get; set; }

        public static string connectionTypeG { get; set; }

        //public Connections(string email, string pwd, string tomail, string attachFile, string subj, string body, string ccmail, string otherAttach, FileInfo file, double size)
        public Connections()
        {
            InitializeComponent();

             if(AddEditConnections.connections != null && AddEditConnections.connections.Count >0)
            {
                GridConnections.DataSource = AddEditConnections.connections;

                this.GridConnections.Columns["ID"].Visible = false;
                this.GridConnections.Columns["IPAddress"].Visible = false;
                this.GridConnections.Columns["IPPort"].Visible = false;
                this.GridConnections.Columns["ContextID"].Visible = false;
                this.GridConnections.Columns["UserName"].Visible = false;
                this.GridConnections.Columns["Password"].Visible = false;
            }

            //Email = email;
            //Password = pwd;
            //attachFileName = attachFile;
            //Subject = subj;
            //Body = body;
            //OtherAttac = otherAttach;
            //fileinfo = file;

        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            isConnection = Helper.checkInterner();
            if (!isConnection)
            {
                isOpening = false;
                isConnection = false;
                isLogging = false;
            }
            else
            {
                isOpening = true;
                isConnection = true;
                isLogging = true;
                connectionTypeG = isConnection ? "TCP/IP":"";
            }
            
            EmailForm emailForm = new Petrojayaforminsert.EmailForm();
            emailForm.Tag = this;
            emailForm.Show(this);
            Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Petro_NoonReports.isNoon)
            {
                Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
                petro_NoonReports.Show();
                this.Close();
            }
            else
            {
                Petro_MorningReports petro_MorningReports = new Petro_MorningReports();
                petro_MorningReports.Show();
                this.Close();
            }
        }

        private void lnkNew_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            btnSend.Enabled = false;
            AddEditConnections addEditConnections = new AddEditConnections();
            addEditConnections.Tag = this;
            addEditConnections.Show(this);
            Hide();
        }
    }

    public class connection
    {
        public int ID { get; set; }

        public string ConnectionName { get; set; }

        public string Terminal { get; set; }

        public string Mailbag { get; set; }

        public string Tatiff { get; set; }

        public string IPAddress { get; set; }

        public int IPPort { get; set; }

        public int ContextID { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
