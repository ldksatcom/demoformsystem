﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using Compressed_Library;
using System.IO;
using System.Diagnostics;
using System.Threading;
using EASendMail;
using DemoFormSystem;


namespace Petrojayaforminsert
{
    public partial class EmailForm : Form
    {
        public static FileInfo textFile { get; set; }

        public static FileInfo AttachmentfileInfo { get; set; }

        public static string printlocations { get; set; }

        public static string _email { get; set; }

        public static string _password { get; set; }

        public static string _body { get; set; }

        public static string attachtoSendfile { get; set; }

        public static string fileforDB { get; set; }

        public static string _to { get; set; }

        public static string _cc { get; set; }

        public static string Emailid { get; set; }

        public static string password { get; set; }

        public static double TotalMsgSize { get; set; }

        public static List<string> whitelist = new List<string>();

        public EmailForm()
        {
            InitializeComponent();

            btnSend.Enabled = true;

            this.SetMailAttachmentSize();

            this.SetMessageSize();

            _email = formLogin.EmailID;
            _password = formLogin.Password;

            txtToEmailid.Text = _to = formLogin.To;

            txtCCMails.Text = _cc = formLogin.CC;

            if (Connections.isOpening)
            {
                lblIsOpening.Text = "✓";
                lblIsOpening.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblIsOpening.Text = "X";
                lblIsOpening.ForeColor = System.Drawing.Color.Red;
            }

            if (Connections.isConnection)
            {
                lblIsConnecting.Text = "✓";
                lblIsConnecting.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblIsConnecting.Text = "X";
                lblIsConnecting.ForeColor = System.Drawing.Color.Red;
            }

            if (Connections.isLogging)
            {
                lblIsLog.Text = "✓";
                lblIsLog.ForeColor = System.Drawing.Color.Green;
            }
            else
            {
                lblIsLog.Text = "X";
                lblIsLog.ForeColor = System.Drawing.Color.Red;
            }

            lblIsCompress.Text = "✓";
            lblIsSave.Text = "✓";
            lblIsFormSave.Text = "✓";

            lblConnectionType.Text = Connections.connectionTypeG;

            if (Petro_NoonReports.isNoon)
            {
                lblCompressedSize.Text = string.Format("Compressed:{0}", Petro_NoonReports.N_CompressedPercentage);
                lblEmailAttach.Text = Petro_NoonReports.N_Filenname;

                prgrsCompressed.Visible = true;
                prgrsCompressed.Show();
                prgrsCompressed.Increment(100);
                lstboxResult.Items.Add(string.Format("Orginal File Size:{0}", Petro_NoonReports.N_originalSize));
                //lstboxResult.Items.Add(string.Format("Text File Size:{0}", formNoonReport.textFileLength));
                lstboxResult.Items.Add(string.Format("Compressed and Encrypt File Size{0}", Petro_NoonReports.N_CompressedByteSize));
                // lstboxResult.Items.Add(string.Format("Compressed File to Text File Size:{0}", Petrojaya_Noon_Report_Form.textFileSize));
                textFile = Petro_NoonReports.N_fileinfoText;
                txtSubject.Text = "Noon_Report";
                printlocations = Petro_NoonReports.N_printFilelocation;
                _body = Petro_NoonReports.N_emailbodystring;
            }
            else
            {
                lblCompressedSize.Text = string.Format("Compressed:{0}", Petro_MorningReports.M_CompressedPercentage);
                lblEmailAttach.Text = Petro_MorningReports.M_Filenname;
                prgrsCompressed.Visible = true;
                prgrsCompressed.Show();
                prgrsCompressed.Increment(100);
                lstboxResult.Items.Add(string.Format("Orginal File Size:{0}", Petro_MorningReports.M_originalSize));
                //lstboxResult.Items.Add(string.Format("Text File Size:{0}", formNoonReport.textFileLength));
                lstboxResult.Items.Add(string.Format("Compressed and Encrypt File Size{0}", Petro_MorningReports.M_CompressedByteSize));
                // lstboxResult.Items.Add(string.Format("Compressed File to Text File Size:{0}", Petrojaya_Noon_Report_Form.textFileSize));
                textFile = Petro_MorningReports.M_fileinfoText;
                txtSubject.Text = "Morning_Report";
                printlocations = Petro_MorningReports.M_printFilelocation;
                _body = Petro_MorningReports.M_emailbodystring;
            }

        }

        public void printPDFWithAcrobat()
        {
            string Filepath = printlocations;
            //if (!string.IsNullOrEmpty(Filepath))
            //{

            //    using (PrintDialog Dialog = new PrintDialog())
            //    {
            //        Dialog.ShowDialog();

            //        ProcessStartInfo printProcessInfo = new ProcessStartInfo()
            //        {
            //            Verb = "print",
            //            CreateNoWindow = true,
            //            FileName = Filepath,
            //            WindowStyle = ProcessWindowStyle.Hidden
            //        };

            //        Process printProcess = new Process();
            //        printProcess.StartInfo = printProcessInfo;
            //        printProcess.Start();

            //        printProcess.WaitForInputIdle();

            //        Thread.Sleep(3000);

            //        if (false == printProcess.CloseMainWindow())
            //        {
            //            printProcess.Kill();
            //        }
            //    }
            //}
        }

        public void SendEmail(string to, string file, string subject, string body, string ccmails, string otherfile)
        {
            try
            {
                SmtpMail oMail = new SmtpMail("TryIt");
                SmtpClient oSmtp = new SmtpClient();

                // Set sender email address, please change it to yours
                oMail.From = _email;

                oMail.To = _to;

                // oMail.To = to;

                // oMail.To = "Shoresmts@ldksatcom.in";



                // Set email subject
                oMail.Subject = subject;

                // Set email body
                oMail.TextBody = body;

                // oMail.AddAttachment(file);

                if (string.IsNullOrEmpty(otherfile))
                {
                    //oMail.AddAttachment(file);

                }
                else
                {
                    // otherfile = txt_OtherAttachments.Text.ToString();

                    otherfile = AttachmentfileInfo.ToString();

                    oMail.AddAttachment(otherfile);

                }

                // Your SMTP server address
                SmtpServer oServer = new SmtpServer("mail.ldksatcom.in");
                // User and password for ESMTP authentication, if your server doesn't require
                // User authentication, please remove the following codes.
                oServer.User = _email;

                oServer.Password = _password;

                string[] arccmails = new string[5];

                if (!string.IsNullOrEmpty(ccmails))
                {
                    if (ccmails.Contains(';'))
                    {
                        arccmails = ccmails.Split(';');
                    }
                    else if (ccmails.Contains(','))
                    {
                        arccmails = ccmails.Split(',');
                    }
                    else
                        arccmails[0] = ccmails;
                }

                foreach (string ccmail in arccmails)
                {
                    if (!string.IsNullOrEmpty(ccmail))
                    {
                        oMail.Cc.Add(new MailAddress(ccmail));
                    }

                }

                // Set recipient email address, please change it to yours
                string[] artomail = new string[3];

                if (!string.IsNullOrEmpty(to))
                {
                    if (to.Contains(';'))
                    {
                        artomail = to.Split(';');
                    }
                    else if (to.Contains(','))
                    {
                        artomail = to.Split(',');
                    }
                    else
                        artomail[0] = to;
                }

                foreach (string tomail in artomail)
                {
                    if (!string.IsNullOrEmpty(tomail))
                    {
                        oMail.To.Add(new MailAddress(tomail));

                    }
                }





                // If your smtp server requires TLS connection, please add this line
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;

                // If your smtp server requires implicit SSL connection on 465 port, please add this line
                //oServer.Port = 465;
                // oServer.ConnectType = SmtpConnectType.ConnectSSLAuto;



                try
                {
                    oSmtp.SendMail(oServer, oMail);
                    lblIsTransfer.Text = "✓";
                    lblIsDisconnect.Text = "✓";
                    lblIsColsed.Text = "✓";

                    MessageBox.Show("email was sent successfully!");
                    clearFields();
                    printPDFWithAcrobat();
                }
                catch (System.Exception ep)
                {
                    MessageBox.Show("failed to send email with the following error:" + ep.Message);
                }
            }
            catch (System.Exception error)
            {
                MessageBox.Show("failed to send email with the following error:" + error.Message);
            }




            #region
            //try
            //{
            //    Outlook.Application oApp = new Outlook.Application();

            //    Outlook.MailItem oMsg = (Outlook.MailItem)oApp.CreateItem(Outlook.OlItemType.olMailItem);

            //    Outlook.Recipients oRecips = (Outlook.Recipients)oMsg.Recipients;

            //    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(to);
            //    oRecip.Resolve();

            //    oMsg.CC = ccmails;

            //    //Add an attachment.
            //    if (file.Contains("|"))
            //    {
            //        string[] attached = file.Split('|');

            //        String sDisplayName = file;
            //        //int iPosition = (int)oMsg.Body.Length + 1;
            //        //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
            //        //now attached the file
            //        Outlook.Attachment oAttach;

            //        foreach (string att in attached)
            //        {
            //            oAttach = oMsg.Attachments.Add(file, Type.Missing, Type.Missing, Type.Missing);
            //        }
            //    }
            //    else
            //    {
            //        //String sDisplayName = file;
            //        //int iPosition = (int)oMsg.Body.Length + 1;
            //        //int iAttachType = (int)Outlook.OlAttachmentType.olByValue;
            //        //now attached the file
            //        Outlook.Attachment oAttach = oMsg.Attachments.Add
            //                                     (file, Type.Missing, Type.Missing, Type.Missing);
            //    }
            //    //Subject line
            //    oMsg.Subject = subject;
            //    // Send.
            //    oMsg.Send();

            //    MessageBox.Show("Email was successfully Send.", "Successs Message", MessageBoxButtons.OK);
            //    clearFields();
            //    printPDFWithAcrobat();
            //}
            //catch (System.Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            #endregion
        }

        //public static Dictionary<string,string> GetEmailToAddress(string To,List<Dictionary<string,string>> lstto)
        //{
        //    Dictionary<string, string> keyvalue = new Dictionary<string, string>();

        //    foreach (Dictionary<string,string> dictTo in lstto)
        //    {
        //        if(dictTo["To"].ToString().Equals(To))
        //        {

        //        }
        //    }
        //    return keyvalue;
        //}

        private void clearFields()
        {
            txtToEmailid.Text = string.Empty;
            txtCCMails.Text = string.Empty;
            txtSubject.Text = string.Empty;

            txt_OtherAttachments.Text = string.Empty;

            lblCompressedSize.Text = string.Empty;
            lstboxResult.Items.Clear();
            prgrsCompressed.Hide();
        }

        private void btnSend_Click_1(object sender, EventArgs e)
        {
            if (!Connections.isConnection)
            {
                bool isConnection = Helper.checkInterner();

                if (!isConnection)
                {
                    MessageBox.Show("The internet not connected, please check the internet connection.");
                    return;
                }
            }

            btnSend.Enabled = false;

            StringBuilder stringBuilder = new StringBuilder();
            int unWhiteList = 0;
            string attachFileName = string.Empty;

            string otherAttachment = string.Empty;

            if (AttachmentfileInfo != null)
            {
                otherAttachment = AttachmentfileInfo.ToString();
            }

            // string otherAttachment = txt_OtherAttachments.Text.ToString();

            if (Petro_NoonReports.isNoon)
                attachFileName = Petro_NoonReports.N_Filenname;
            else
                attachFileName = Petro_MorningReports.M_Filenname;

            FileInfo infoFileForm = new FileInfo(attachFileName);

            FileInfo infoAttachFile = null;

            if (!string.IsNullOrEmpty(otherAttachment))

                infoAttachFile = new FileInfo(AttachmentfileInfo.ToString());

            // Get combobox selection (in handler)
            int comboValue = 0;

            string maxAttachSize = Convert.ToString(((KeyValuePair<int, string>)this.AttachmentSize_ComboBox.SelectedItem).Value);
            string[] split = maxAttachSize.Split(' ');
            if (split[1].Equals("MB"))
                comboValue = ((Convert.ToInt32(split[0]) * 1000) * 1024);
            else
                comboValue = Convert.ToInt32(split[0]) * 1024;

            int MsgValue = 0;

            string maxMsgSize = Convert.ToString(((KeyValuePair<int, string>)this.MessageSize_Combo.SelectedItem).Value);
            split = maxMsgSize.Split(' ');
            if (split[1].Equals("MB"))
                MsgValue = ((Convert.ToInt32(split[0]) * 1000) * 1024);
            else
                MsgValue = Convert.ToInt32(split[0]) * 1024;

            double totalAttachsize = 0;

            if (infoAttachFile != null)
                totalAttachsize = infoFileForm.Length + infoAttachFile.Length;
            else
                totalAttachsize = infoFileForm.Length;

            if (totalAttachsize < comboValue)
            {
                if (!string.IsNullOrEmpty(txtToEmailid.Text))
                {
                    double Wholemessagesize;

                    TotalMsgSize = Wholemessagesize = GetWholeEmailTextSize(totalAttachsize);

                    if (Wholemessagesize < MsgValue)
                    {
                        if (File.Exists(otherAttachment))
                        {
                            //if (Petro_NoonReports.isNoon)
                            //    Helper.StoreFileToDatabase(fileforDB, Petro_NoonReports.N_ReportID);
                            //else
                            //    Helper.StoreFileToDatabase(fileforDB, Petro_MorningReports.M_ReportID);

                            string formDirctory = string.Empty;
                            if (!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                                formDirctory = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
                            else
                                formDirctory = @"C:\FormSystemReport";

                            ExportExcel.addAttachfileToexcel(formDirctory + "\\ReportData.xlsx", fileforDB);
                        }
                        else
                        {

                        }
                        getWhitelistEmail();
                        List<string> lstCCmail = new List<string>();
                        if (whitelist.Count > 0)
                        {
                            if (!whitelist.Contains(txtToEmailid.Text))
                            {
                                MessageBox.Show(string.Format("Can't send email, this mail {0} doesn't in the whitelist.", txtToEmailid.Text));
                                btnSend.Enabled = true;
                                return;
                            }
                            else 
                            {
                                string[] arccmails = new string[5];
                                string ccmails = txtCCMails.Text;

                                if (!string.IsNullOrEmpty(ccmails))
                                {
                                    if (ccmails.Contains(';'))
                                    {
                                        arccmails = ccmails.Split(';');
                                    }
                                    else if (ccmails.Contains(','))
                                    {
                                        arccmails = ccmails.Split(',');
                                    }
                                    else
                                        arccmails[0] = ccmails;
                                }

                                foreach(string ccmail in arccmails)
                                {
                                    if (!string.IsNullOrEmpty(ccmail))
                                    {
                                        if (whitelist.Contains(ccmail))
                                        {
                                            lstCCmail.Add(ccmail);
                                        }
                                        else
                                        {
                                            unWhiteList++;
                                            stringBuilder.Append(ccmail);
                                        }
                                    }
                                }
                                btnSend.Enabled = true;
                            }

                            Helper.SendEmail(_email, _password, txtToEmailid.Text, attachFileName, txtSubject.Text, _body, lstCCmail, otherAttachment, AttachmentfileInfo);
                            lblIsTransfer.Text = "✓";
                            lblIsTransfer.ForeColor = System.Drawing.Color.Green;
                            lblIsDisconnect.Text = "✓";
                            lblIsDisconnect.ForeColor = System.Drawing.Color.Green;
                            lblIsColsed.Text = "✓";
                            lblIsColsed.ForeColor = System.Drawing.Color.Green;
                            if (unWhiteList > 0)
                                MessageBox.Show(string.Format("Email Sent Successfully and Not send this mails {0}, it doesn't in whitelist.", stringBuilder.ToString()));
                            else
                                MessageBox.Show("Email Sent Successfully");

                            btnSend.Enabled = true;
                            clearFields();
                            printPDFWithAcrobat();
                        }
                        else
                        {
                            btnSend.Enabled = true;
                            MessageBox.Show(string.Format("Can't send email, this mail {0} doesn't in the whitelist.", txtToEmailid.Text));
                            lblIsTransfer.Text = "X";
                            lblIsTransfer.ForeColor = System.Drawing.Color.Red;
                            lblIsDisconnect.Text = "X";
                            lblIsDisconnect.ForeColor = System.Drawing.Color.Red;
                            lblIsColsed.Text = "X";
                            lblIsColsed.ForeColor = System.Drawing.Color.Red;
                            return;
                        }
                    }
                    else
                    {
                        btnSend.Enabled = true;
                        MessageBox.Show(string.Format("Email Size is greater than {0}", MsgValue));
                        lblIsTransfer.Text = "X";
                        lblIsTransfer.ForeColor = System.Drawing.Color.Red;
                        lblIsDisconnect.Text = "X";
                        lblIsDisconnect.ForeColor = System.Drawing.Color.Red;
                        lblIsColsed.Text = "X";
                        lblIsColsed.ForeColor = System.Drawing.Color.Red;
                    }
                }
                else
                {
                    btnSend.Enabled = true;
                    MessageBox.Show("Email is incorrect, please enter valid email id.");

                    lblIsTransfer.Text = "X";
                    lblIsTransfer.ForeColor = System.Drawing.Color.Red;
                    lblIsDisconnect.Text = "X";
                    lblIsDisconnect.ForeColor = System.Drawing.Color.Red;
                    lblIsColsed.Text = "X";
                    lblIsColsed.ForeColor = System.Drawing.Color.Red;
                }
            }
            else
            {
                btnSend.Enabled = true;
                MessageBox.Show(string.Format("Email Attachment is greater than {0}, please select the file size within {0}", ((KeyValuePair<int, string>)this.AttachmentSize_ComboBox.SelectedItem).Value));
                lblIsTransfer.Text = "X";
                lblIsTransfer.ForeColor = System.Drawing.Color.Red;
                lblIsDisconnect.Text = "X";
                lblIsDisconnect.ForeColor = System.Drawing.Color.Red;
                lblIsColsed.Text = "X";
                lblIsColsed.ForeColor = System.Drawing.Color.Red;
            }
        }

        private void btnClear_LinkClicked_1(object sender, LinkLabelLinkClickedEventArgs e)
        {
            clearFields();
        }

        private void btnEditForm_Click_1(object sender, EventArgs e)
        {
            string sql = ConfigurationSettings.AppSettings["ConnectionString"].ToString();
            SqlConnection sqlcon = new SqlConnection(sql);
            SqlCommand cmd = new SqlCommand("select * from NoonReportT", sqlcon);
            sqlcon.Open();

            SqlDataReader dr = cmd.ExecuteReader();
            //if (dr.Read())
            //{
            //    txtVoyage.Text = dr.GetValue(0).ToString();
            //    txtShipname.Text = dr.GetValue(1).ToString();
            //}
            sqlcon.Close();
            dr.Close();
        }

        private void SetMailAttachmentSize()
        {
            try
            {
                Dictionary<int, string> keyValuePairs = new Dictionary<int, string>();
                keyValuePairs.Add(3, "3 KB");
                keyValuePairs.Add(5, "5 KB");
                keyValuePairs.Add(10, "10 KB");
                keyValuePairs.Add(20, "15 KB");
                keyValuePairs.Add(1, "1 MB");
                this.AttachmentSize_ComboBox.DataSource = new BindingSource(keyValuePairs, null);
                this.AttachmentSize_ComboBox.DisplayMember = "Value";
                this.AttachmentSize_ComboBox.ValueMember = "Key";
            }
            catch (Exception eror)
            {

            }
        }

        private void SetMessageSize()
        {
            try
            {
                Dictionary<int, string> MessageSizeValue = new Dictionary<int, string>();
                MessageSizeValue.Add(1, "1 KB");
                MessageSizeValue.Add(2, "3 KB");
                MessageSizeValue.Add(3, "5 KB");
                MessageSizeValue.Add(4, "8 KB");
                MessageSizeValue.Add(5, "10 KB");
                MessageSizeValue.Add(6, "15 KB");
                MessageSizeValue.Add(7, "20 KB");
                MessageSizeValue.Add(8, "40 KB");
                MessageSizeValue.Add(9, "60 KB");
                MessageSizeValue.Add(10, "1 MB");
                MessageSizeValue.Add(11, "1.5 MB");
                MessageSizeValue.Add(12, "2 MB");

                this.MessageSize_Combo.DataSource = new BindingSource(MessageSizeValue, null);
                this.MessageSize_Combo.DisplayMember = "Value";
                this.MessageSize_Combo.ValueMember = "Key";
            }
            catch (Exception e)
            { }
        }

        public double GetWholeEmailTextSize(double attachmentsSize)
        {
            //int iLine = txtBody.GetLineFromCharIndex(txtBody.TextLength - 1) + 1;
            //int iHeight = TextRenderer.MeasureText(this.txtBody.Text, this.txtBody.Font).Height;
            //float fTextHeight = iHeight * ((float)iLine + 0.25f);
            //txtBody.Size = new Size(txtBody.Size.Width, (int)fTextHeight);

            byte[] EmailBodySize = Encoding.ASCII.GetBytes(_body);

            double OriginalBodySize = EmailBodySize.Length;

            byte[] EmailCcSize = Encoding.ASCII.GetBytes(txtCCMails.Text);

            double OriginalCcSize = EmailCcSize.Length;

            byte[] EmailSubjectSize = Encoding.ASCII.GetBytes(txtSubject.Text);

            double OriginalSubjectSize = EmailSubjectSize.Length;

            byte[] ToEmailid = Encoding.ASCII.GetBytes(txtToEmailid.Text);

            double OriginalToEmailid = ToEmailid.Length;

            byte[] EmailFromIdSize = Encoding.ASCII.GetBytes(formLogin.EmailID);

            double OriginalFromIdSize = EmailFromIdSize.Length;

            double WholeMessageSizeValue = attachmentsSize + OriginalBodySize + OriginalCcSize + OriginalSubjectSize + OriginalToEmailid + OriginalFromIdSize;

            return WholeMessageSizeValue;
        }

        private void btn_browse_Click(object sender, EventArgs e)
        {
            openFileDialog1.Multiselect = false;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                string[] validFileTypes = { "bmp", "doc", "xlsx", "png", "jpg", "jpeg", "gif", "xls", "txt", "pdf", "mp3", "mp4", "wav" };

                string ext = System.IO.Path.GetExtension(openFileDialog1.FileName);

                string fileName = Path.GetFileName(openFileDialog1.FileName);

                string filePath = openFileDialog1.FileName;

                Remove_LinkLabel.Visible = true;

                bool isValid = false;

                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValid = true;
                        break;
                    }
                }

                if (!isValid)
                {
                    MessageBox.Show("Invalid File.Please Choose a File with Extension" + string.Join(",", validFileTypes));
                }
                else
                {
                    string filenamewithoutEx = Path.GetFileNameWithoutExtension(openFileDialog1.FileName);
                    FileInfo originalfileInfo = new FileInfo(openFileDialog1.FileName);
                    string attachOriginalSize = BytestoKB.SizeSuffix(originalfileInfo.Length);

                    lstboxResult.Items.Add(string.Format("Original file size of attachments:{0}", attachOriginalSize));

                    string zipFilename = Helper.CompressedZIP(openFileDialog1.FileName, filenamewithoutEx);

                    fileforDB = zipFilename;
                    AttachmentfileInfo = new FileInfo(zipFilename);
                    string zipfileSize = BytestoKB.SizeSuffix(AttachmentfileInfo.Length);
                    lstboxResult.Items.Add(string.Format("Compressed file size of attachments:{0}", zipfileSize));

                    txt_OtherAttachments.Text = Path.GetFileNameWithoutExtension(AttachmentfileInfo.FullName);
                }
            }
        }

        private void Remove_LinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            txt_OtherAttachments.Text = string.Empty;
            Remove_LinkLabel.Visible = false;
        }

        private void lnkCancel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.Close();
            if (Petro_MorningReports.isMoring)
            {
                Petro_MorningReports obj = new Petro_MorningReports();
                obj.Show();
            }
            else
            {
                Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
                petro_NoonReports.Show();
            }
        }

        private void lnkDownload_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string formDirctory = string.Empty;

            formDirctory = @"C:\SMTSReport";
            if (!Directory.Exists(formDirctory))
                Directory.CreateDirectory(formDirctory);

            formDirctory += "/OriginalFormFile.txt";
            File.WriteAllText(formDirctory, File.ReadAllText(textFile.FullName));

            formDirctory = string.Empty;
            formDirctory = @"C:\SMTSReport";
            formDirctory += "/CompressedFile.txt";
            File.WriteAllText(formDirctory, File.ReadAllText(lblEmailAttach.Text));
        }

        private void btnAddBlockList_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtBlockList.Text))
            {
                MessageBox.Show("Please enter the value in White List box.");
                return;
            }
            String line;
            string formDirctory = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                    formDirctory = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
                else
                    formDirctory = @"C:\FormSystemReport";

                if (!Directory.Exists(formDirctory))
                    Directory.CreateDirectory(formDirctory);

                formDirctory += "\\WhiteLists.txt";

                if (!File.Exists(formDirctory))
                {
                    File.WriteAllText(formDirctory, "");
                }

                //Pass the file path and file name to the StreamReader constructor
                using (StreamReader sr = new StreamReader(formDirctory))
                {
                    //Read the first line of text
                    line = sr.ReadLine();
                    //Continue to read until you reach end of file
                    //while (! string.IsNullOrEmpty(line))
                    //{
                    //    //write the lie to console window
                    //    //Read the next line
                    //    line += sr.ReadLine();
                    //}
                    //close the file
                    sr.Close();
                }

                if (!string.IsNullOrEmpty(line))
                {
                    line += txtBlockList.Text + ",";
                }
                else
                    line = txtBlockList.Text + ",";

                //Pass the filepath and filename to the StreamWriter Constructor
                using (StreamWriter sw = new StreamWriter(formDirctory))
                {
                    //Write a line of text
                    sw.WriteLine(line);

                    //Close the file
                    sw.Close();

                    MessageBox.Show("The email is added successfully to whitelist.");
                    txtBlockList.Text = string.Empty;
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("The {0} access path is denited, try give access permission or login amdin user.", formDirctory));
            }
        }

        //private void lnkViewtext_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        //{
        //    string formDirctory = string.Empty;

        //    formDirctory = @"C:\SMTSReport";
        //    if (!Directory.Exists(formDirctory))
        //        Directory.CreateDirectory(formDirctory);

        //    formDirctory += "/CompressedFile.txt";
        //    File.WriteAllText(formDirctory, File.ReadAllText(lblAttachements.Text));
        //}

        //private void lnkEditForm_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        //{
        //    string sql = ConfigurationSettings.AppSettings["ConnectionString"].ToString();
        //    SqlConnection sqlcon = new SqlConnection(sql);
        //    SqlCommand cmd = new SqlCommand("select * from NoonReportT", sqlcon);
        //    sqlcon.Open();

        //    SqlDataReader dr = cmd.ExecuteReader();
        //    //if (dr.Read())
        //    //{
        //    //    txtVoyage.Text = dr.GetValue(0).ToString();
        //    //    txtShipname.Text = dr.GetValue(1).ToString();
        //    //}
        //    sqlcon.Close();
        //    dr.Close();
        //}

        public static void getWhitelistEmail()
        {
            string formDirctory, line = string.Empty;

            if (!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                formDirctory = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
            else
                formDirctory = @"C:\FormSystemReport";

            if (!Directory.Exists(formDirctory))
                Directory.CreateDirectory(formDirctory);

            formDirctory += "\\WhiteLists.txt";

            try
            {
                if (!File.Exists(formDirctory))
                {
                    return;
                }

                //Pass the file path and file name to the StreamReader constructor
                using (StreamReader sr = new StreamReader(formDirctory))
                {
                    //Read the first line of text
                    line = sr.ReadLine();
                    //Continue to read until you reach end of file
                    //while (line != null)
                    //{
                    //    //write the lie to console window
                    //    //Read the next line
                    //    line += sr.ReadLine();
                    //}
                    //close the file
                    sr.Close();
                }

                if (!string.IsNullOrEmpty(line))
                {
                    if (line.Contains(','))
                    {
                        string[] blocklistemails = line.Split(',');
                        foreach (var email in blocklistemails)
                        {
                            if (!string.IsNullOrEmpty(email))
                                whitelist.Add(email);
                        }
                    }
                    else
                    {
                        whitelist.Add(line);
                    }
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(string.Format("The {0} access path is denited, try give access permission or login amdin user.", formDirctory));
            }

        }
    }
}
