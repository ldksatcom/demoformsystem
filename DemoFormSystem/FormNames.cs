﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class FormNames : Form
    {
        public static string VesselName { get; set; }
        List<Dictionary<string, string>> lstcrew { get; set; }

        public FormNames(List<Dictionary<string, string>> lstcrewpa = null)
        {
            InitializeComponent();
            lstcrew = lstcrewpa;
            lblCompanyName.Text = formLogin.CompanyName + " "+ "Form System";
            this.DisplayFormReports();
        }


        private void DisplayFormReports()
        {
            try
            {
                Dictionary<int, string> keyValues = Login_DataBase.GetVesselName(formLogin.CompanyName, lstcrew);

                this.Display_formcombo.DataSource = new BindingSource(keyValues, null);
                this.Display_formcombo.DisplayMember = "Value";
                this.Display_formcombo.ValueMember = "Key";

                Display_formcombo.Font = new Font(Display_formcombo.Font.Name, 13, Display_formcombo.Font.Style, Display_formcombo.Font.Unit);
            }
            catch(Exception e)
            {

            }
        }

        private void Display_formcombo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(Display_formcombo.SelectedIndex != 0)
            {
                VesselName = Convert.ToString(Display_formcombo.Text);
                DashBoard dashboard = new DashBoard();
                this.Hide();
                dashboard.ShowDialog();
            }
        }

        private void btn_FormClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
