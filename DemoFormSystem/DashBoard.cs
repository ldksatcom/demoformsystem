﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class DashBoard : Form
    {
        public DashBoard()
        {
            InitializeComponent();
            lblCompanyName.Text = formLogin.CompanyName + " " +"Form System";
        }

        private void pnlMorning_MouseHover(object sender, EventArgs e)
        {
           // pnlMorning.BackColor = Color.AliceBlue;
            lblMorningDashBoard.ForeColor = Color.WhiteSmoke;
        }

        private void pnlNoon_MouseHover(object sender, EventArgs e)
        {
            //pnlNoon.BackColor = Color.LightBlue;
            lblNoonDashboard.ForeColor = Color.WhiteSmoke;
        }

        private void pnlMorning_MouseLeave(object sender, EventArgs e)
        {
            //pnlNoon.BackColor = Color.MidnightBlue;
            lblNoonDashboard.ForeColor = Color.Black;
        }

        private void pnlNoon_MouseLeave(object sender, EventArgs e)
        {
            lblNoonDashboard.ForeColor = Color.Black;
        }

        private void pnlMorning_Click(object sender, EventArgs e)
        {
            Petro_MorningReports petro_MorningReports = new Petro_MorningReports();
            this.Hide();
            petro_MorningReports.Show();
        }

        private void pnlNoon_Click(object sender, EventArgs e)
        {
            Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
            this.Hide();
            petro_NoonReports.Show();
        }

        private void lblMorningDashBoard_Click(object sender, EventArgs e)
        {
            Petro_MorningReports petro_MorningReports = new Petro_MorningReports();
            this.Hide();
            petro_MorningReports.Show();
        }

        private void lblNoonDashboard_Click(object sender, EventArgs e)
        {
            Petro_NoonReports petro_NoonReports = new Petro_NoonReports();
            this.Hide();
            petro_NoonReports.Show();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btn_Home_Click(object sender, EventArgs e)
        {
            FormNames DashBoard = new FormNames();
            this.Hide();
            DashBoard.Show();
        }
    }
}
