﻿namespace DemoFormSystem
{
    partial class Petro_NoonReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Petro_NoonReports));
            this.pnlPrint = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMrgQMail = new System.Windows.Forms.Label();
            this.btn_reply = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.LoadNoonReport = new System.Windows.Forms.PictureBox();
            this.lblisUpdated = new System.Windows.Forms.Label();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnBackIn = new System.Windows.Forms.Button();
            this.btnPrint = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.loadbtn = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label56 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.txtLOMainengineSump = new System.Windows.Forms.TextBox();
            this.txtLOMainengineStoragetank = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.txtBunkerReceivedMFO = new System.Windows.Forms.TextBox();
            this.txtBunkerReceivedMGO = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.txtBunkerRobMFO = new System.Windows.Forms.TextBox();
            this.txtBunkerRobMGO = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.txtMGOmainengine = new System.Windows.Forms.TextBox();
            this.txtMGOauxiliaryengine = new System.Windows.Forms.TextBox();
            this.txtMGOboiler = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.txtMFOmainengine = new System.Windows.Forms.TextBox();
            this.txtMFOboiler = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.txtAvgSpeed = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtVoyageSpeed = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtVoyage = new System.Windows.Forms.TextBox();
            this.txtShipname = new System.Windows.Forms.TextBox();
            this.txtCallsign = new System.Windows.Forms.TextBox();
            this.DP1 = new System.Windows.Forms.DateTimePicker();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtLongitude = new System.Windows.Forms.TextBox();
            this.txtTime = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLatitude = new System.Windows.Forms.TextBox();
            this.txtRemarks = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.txtVSLHeading = new System.Windows.Forms.TextBox();
            this.txtCourseTime = new System.Windows.Forms.TextBox();
            this.txtRemarks1 = new System.Windows.Forms.TextBox();
            this.lblNoonReport = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblCompanyName = new System.Windows.Forms.Label();
            this.printdocNoonReport = new System.Drawing.Printing.PrintDocument();
            this.pnlPrint.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoadNoonReport)).BeginInit();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlPrint
            // 
            this.pnlPrint.Controls.Add(this.panel1);
            this.pnlPrint.Controls.Add(this.panel2);
            this.pnlPrint.Location = new System.Drawing.Point(2, -3);
            this.pnlPrint.Name = "pnlPrint";
            this.pnlPrint.Size = new System.Drawing.Size(917, 539);
            this.pnlPrint.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblMrgQMail);
            this.panel1.Controls.Add(this.btn_reply);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.LoadNoonReport);
            this.panel1.Controls.Add(this.lblisUpdated);
            this.panel1.Controls.Add(this.btnUpdate);
            this.panel1.Controls.Add(this.btnBackIn);
            this.panel1.Controls.Add(this.btnPrint);
            this.panel1.Controls.Add(this.btnClose);
            this.panel1.Controls.Add(this.loadbtn);
            this.panel1.Controls.Add(this.btnSave);
            this.panel1.Controls.Add(this.tableLayoutPanel14);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.tableLayoutPanel13);
            this.panel1.Controls.Add(this.label52);
            this.panel1.Controls.Add(this.tableLayoutPanel12);
            this.panel1.Controls.Add(this.label49);
            this.panel1.Controls.Add(this.tableLayoutPanel11);
            this.panel1.Controls.Add(this.label45);
            this.panel1.Controls.Add(this.tableLayoutPanel10);
            this.panel1.Controls.Add(this.label41);
            this.panel1.Controls.Add(this.tableLayoutPanel5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.tableLayoutPanel2);
            this.panel1.Location = new System.Drawing.Point(3, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(911, 467);
            this.panel1.TabIndex = 3;
            // 
            // lblMrgQMail
            // 
            this.lblMrgQMail.AutoSize = true;
            this.lblMrgQMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMrgQMail.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblMrgQMail.Location = new System.Drawing.Point(321, 397);
            this.lblMrgQMail.Name = "lblMrgQMail";
            this.lblMrgQMail.Size = new System.Drawing.Size(12, 16);
            this.lblMrgQMail.TabIndex = 124;
            this.lblMrgQMail.Text = ".";
            // 
            // btn_reply
            // 
            this.btn_reply.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_reply.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_reply.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_reply.Location = new System.Drawing.Point(547, 426);
            this.btn_reply.Name = "btn_reply";
            this.btn_reply.Size = new System.Drawing.Size(75, 29);
            this.btn_reply.TabIndex = 117;
            this.btn_reply.Text = "Reply";
            this.btn_reply.UseVisualStyleBackColor = false;
            this.btn_reply.Click += new System.EventHandler(this.btn_reply_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(242, 397);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 13);
            this.label1.TabIndex = 125;
            this.label1.Text = "Queue Mails:";
            // 
            // LoadNoonReport
            // 
            this.LoadNoonReport.Image = ((System.Drawing.Image)(resources.GetObject("LoadNoonReport.Image")));
            this.LoadNoonReport.Location = new System.Drawing.Point(447, 166);
            this.LoadNoonReport.Name = "LoadNoonReport";
            this.LoadNoonReport.Size = new System.Drawing.Size(100, 74);
            this.LoadNoonReport.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.LoadNoonReport.TabIndex = 116;
            this.LoadNoonReport.TabStop = false;
            this.LoadNoonReport.Visible = false;
            // 
            // lblisUpdated
            // 
            this.lblisUpdated.AutoSize = true;
            this.lblisUpdated.Location = new System.Drawing.Point(4, 380);
            this.lblisUpdated.Name = "lblisUpdated";
            this.lblisUpdated.Size = new System.Drawing.Size(0, 13);
            this.lblisUpdated.TabIndex = 115;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnUpdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUpdate.ForeColor = System.Drawing.SystemColors.Window;
            this.btnUpdate.Location = new System.Drawing.Point(646, 426);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 28);
            this.btnUpdate.TabIndex = 114;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnBackIn
            // 
            this.btnBackIn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnBackIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBackIn.ForeColor = System.Drawing.SystemColors.Window;
            this.btnBackIn.Location = new System.Drawing.Point(3, 427);
            this.btnBackIn.Name = "btnBackIn";
            this.btnBackIn.Size = new System.Drawing.Size(75, 28);
            this.btnBackIn.TabIndex = 113;
            this.btnBackIn.Text = "Back";
            this.btnBackIn.UseVisualStyleBackColor = false;
            this.btnBackIn.Click += new System.EventHandler(this.btnBackIn_Click);
            // 
            // btnPrint
            // 
            this.btnPrint.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrint.ForeColor = System.Drawing.SystemColors.Window;
            this.btnPrint.Location = new System.Drawing.Point(740, 426);
            this.btnPrint.Name = "btnPrint";
            this.btnPrint.Size = new System.Drawing.Size(75, 28);
            this.btnPrint.TabIndex = 112;
            this.btnPrint.Text = "Print";
            this.btnPrint.UseVisualStyleBackColor = false;
            this.btnPrint.Click += new System.EventHandler(this.btnPrint_Click);
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.Window;
            this.btnClose.Location = new System.Drawing.Point(827, 422);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 32);
            this.btnClose.TabIndex = 111;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = false;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // loadbtn
            // 
            this.loadbtn.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.loadbtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadbtn.ForeColor = System.Drawing.SystemColors.Window;
            this.loadbtn.Location = new System.Drawing.Point(410, 426);
            this.loadbtn.Name = "loadbtn";
            this.loadbtn.Size = new System.Drawing.Size(117, 29);
            this.loadbtn.TabIndex = 109;
            this.loadbtn.Text = "Load";
            this.loadbtn.UseVisualStyleBackColor = false;
            this.loadbtn.Click += new System.EventHandler(this.loadbtn_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.SystemColors.Window;
            this.btnSave.Location = new System.Drawing.Point(242, 426);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(160, 29);
            this.btnSave.TabIndex = 107;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.label56, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label57, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.txtLOMainengineSump, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.txtLOMainengineStoragetank, 1, 1);
            this.tableLayoutPanel14.Location = new System.Drawing.Point(242, 354);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(666, 40);
            this.tableLayoutPanel14.TabIndex = 31;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(3, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(135, 13);
            this.label56.TabIndex = 0;
            this.label56.Text = "Main Engine system (sump)";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(336, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(169, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Main Engine system (storage tank)";
            // 
            // txtLOMainengineSump
            // 
            this.txtLOMainengineSump.Location = new System.Drawing.Point(3, 18);
            this.txtLOMainengineSump.Name = "txtLOMainengineSump";
            this.txtLOMainengineSump.Size = new System.Drawing.Size(327, 20);
            this.txtLOMainengineSump.TabIndex = 31;
            // 
            // txtLOMainengineStoragetank
            // 
            this.txtLOMainengineStoragetank.Location = new System.Drawing.Point(336, 18);
            this.txtLOMainengineStoragetank.Name = "txtLOMainengineStoragetank";
            this.txtLOMainengineStoragetank.Size = new System.Drawing.Size(324, 20);
            this.txtLOMainengineStoragetank.TabIndex = 32;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(4, 360);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(85, 13);
            this.label7.TabIndex = 30;
            this.label7.Text = "LO Consumed";
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Controls.Add(this.label53, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label54, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtBunkerReceivedMFO, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.txtBunkerReceivedMGO, 1, 1);
            this.tableLayoutPanel13.Location = new System.Drawing.Point(242, 310);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(666, 41);
            this.tableLayoutPanel13.TabIndex = 29;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(3, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(186, 13);
            this.label53.TabIndex = 0;
            this.label53.Text = "MFO. If no bunker received, put \"NA\"";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(336, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(188, 13);
            this.label54.TabIndex = 1;
            this.label54.Text = "MGO. If no bunker received, put \"NA\"";
            // 
            // txtBunkerReceivedMFO
            // 
            this.txtBunkerReceivedMFO.Location = new System.Drawing.Point(3, 18);
            this.txtBunkerReceivedMFO.Name = "txtBunkerReceivedMFO";
            this.txtBunkerReceivedMFO.Size = new System.Drawing.Size(327, 20);
            this.txtBunkerReceivedMFO.TabIndex = 29;
            // 
            // txtBunkerReceivedMGO
            // 
            this.txtBunkerReceivedMGO.Location = new System.Drawing.Point(336, 18);
            this.txtBunkerReceivedMGO.Name = "txtBunkerReceivedMGO";
            this.txtBunkerReceivedMGO.Size = new System.Drawing.Size(324, 20);
            this.txtBunkerReceivedMGO.TabIndex = 30;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(4, 312);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(105, 13);
            this.label52.TabIndex = 28;
            this.label52.Text = "Bunker Received";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.txtBunkerRobMFO, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.txtBunkerRobMGO, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.label50, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label51, 1, 0);
            this.tableLayoutPanel12.Location = new System.Drawing.Point(242, 268);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(666, 38);
            this.tableLayoutPanel12.TabIndex = 27;
            // 
            // txtBunkerRobMFO
            // 
            this.txtBunkerRobMFO.Location = new System.Drawing.Point(3, 16);
            this.txtBunkerRobMFO.Name = "txtBunkerRobMFO";
            this.txtBunkerRobMFO.Size = new System.Drawing.Size(327, 20);
            this.txtBunkerRobMFO.TabIndex = 27;
            // 
            // txtBunkerRobMGO
            // 
            this.txtBunkerRobMGO.Location = new System.Drawing.Point(336, 16);
            this.txtBunkerRobMGO.Name = "txtBunkerRobMGO";
            this.txtBunkerRobMGO.Size = new System.Drawing.Size(324, 20);
            this.txtBunkerRobMGO.TabIndex = 28;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(3, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(188, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Total MFO (Service and settling tanks)";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(336, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(190, 13);
            this.label51.TabIndex = 0;
            this.label51.Text = "Total MGO (Service and settling tanks)";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(4, 271);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(77, 13);
            this.label49.TabIndex = 26;
            this.label49.Text = "Bunker ROB";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 3;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Controls.Add(this.label46, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label47, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.label48, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtMGOmainengine, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtMGOauxiliaryengine, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtMGOboiler, 2, 1);
            this.tableLayoutPanel11.Location = new System.Drawing.Point(242, 226);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(666, 37);
            this.tableLayoutPanel11.TabIndex = 25;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(84, 13);
            this.label46.TabIndex = 0;
            this.label46.Text = "For Main Engine";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(225, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(104, 13);
            this.label47.TabIndex = 0;
            this.label47.Text = "For Auxiliary Engines";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(447, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(51, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "For Boiler";
            // 
            // txtMGOmainengine
            // 
            this.txtMGOmainengine.Location = new System.Drawing.Point(3, 16);
            this.txtMGOmainengine.Name = "txtMGOmainengine";
            this.txtMGOmainengine.Size = new System.Drawing.Size(216, 20);
            this.txtMGOmainengine.TabIndex = 24;
            // 
            // txtMGOauxiliaryengine
            // 
            this.txtMGOauxiliaryengine.Location = new System.Drawing.Point(225, 16);
            this.txtMGOauxiliaryengine.Name = "txtMGOauxiliaryengine";
            this.txtMGOauxiliaryengine.Size = new System.Drawing.Size(216, 20);
            this.txtMGOauxiliaryengine.TabIndex = 25;
            // 
            // txtMGOboiler
            // 
            this.txtMGOboiler.Location = new System.Drawing.Point(447, 16);
            this.txtMGOboiler.Name = "txtMGOboiler";
            this.txtMGOboiler.Size = new System.Drawing.Size(213, 20);
            this.txtMGOboiler.TabIndex = 26;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(4, 231);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(111, 13);
            this.label45.TabIndex = 23;
            this.label45.Text = "MGO Consumption";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 5;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.94737F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.31579F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.05263F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.47368F));
            this.tableLayoutPanel10.Controls.Add(this.label42, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.label43, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.label44, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtMFOmainengine, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtMFOboiler, 4, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(242, 199);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(666, 24);
            this.tableLayoutPanel10.TabIndex = 22;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(3, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(84, 13);
            this.label42.TabIndex = 0;
            this.label42.Text = "For Main Engine";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(347, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(51, 13);
            this.label43.TabIndex = 0;
            this.label43.Text = "For Boiler";
            // 
            // label44
            // 
            this.label44.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label44.Location = new System.Drawing.Point(315, 11);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(26, 13);
            this.label44.TabIndex = 0;
            this.label44.Text = "MT";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMFOmainengine
            // 
            this.txtMFOmainengine.Location = new System.Drawing.Point(123, 3);
            this.txtMFOmainengine.Name = "txtMFOmainengine";
            this.txtMFOmainengine.Size = new System.Drawing.Size(186, 20);
            this.txtMFOmainengine.TabIndex = 22;
            // 
            // txtMFOboiler
            // 
            this.txtMFOboiler.Location = new System.Drawing.Point(429, 3);
            this.txtMFOboiler.Name = "txtMFOboiler";
            this.txtMFOboiler.Size = new System.Drawing.Size(231, 20);
            this.txtMFOboiler.TabIndex = 23;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(6, 196);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(108, 13);
            this.label41.TabIndex = 14;
            this.label41.Text = "MFO consumption";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 4;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel5.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtAvgSpeed, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.label25, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtVoyageSpeed, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.label23, 1, 1);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(242, 151);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(666, 42);
            this.tableLayoutPanel5.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(129, 13);
            this.label21.TabIndex = 0;
            this.label21.Text = "Average Speed (At Noon)";
            // 
            // txtAvgSpeed
            // 
            this.txtAvgSpeed.Location = new System.Drawing.Point(3, 18);
            this.txtAvgSpeed.Name = "txtAvgSpeed";
            this.txtAvgSpeed.Size = new System.Drawing.Size(282, 20);
            this.txtAvgSpeed.TabIndex = 12;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(337, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(151, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "For voyage since last full away";
            // 
            // label25
            // 
            this.label25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label25.Location = new System.Drawing.Point(627, 18);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(36, 27);
            this.label25.TabIndex = 0;
            this.label25.Text = "Knots";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtVoyageSpeed
            // 
            this.txtVoyageSpeed.Location = new System.Drawing.Point(337, 18);
            this.txtVoyageSpeed.Name = "txtVoyageSpeed";
            this.txtVoyageSpeed.Size = new System.Drawing.Size(282, 20);
            this.txtVoyageSpeed.TabIndex = 13;
            // 
            // label23
            // 
            this.label23.Dock = System.Windows.Forms.DockStyle.Left;
            this.label23.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.label23.Location = new System.Drawing.Point(290, 15);
            this.label23.Margin = new System.Windows.Forms.Padding(0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(34, 30);
            this.label23.TabIndex = 0;
            this.label23.Text = "Knots";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(4, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Speed";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.07735F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.92265F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblNoonReport, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.82282F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.62162F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.81448F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(905, 152);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.10306F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.49437F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.37217F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.78964F));
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtVoyage, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtShipname, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCallsign, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.DP1, 3, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(239, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 44.89796F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 55.10204F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(663, 47);
            this.tableLayoutPanel1.TabIndex = 4;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(332, 21);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Call Sign";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(332, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Date";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Voyage No";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Ship Name";
            // 
            // txtVoyage
            // 
            this.txtVoyage.Location = new System.Drawing.Point(110, 3);
            this.txtVoyage.Name = "txtVoyage";
            this.txtVoyage.Size = new System.Drawing.Size(216, 20);
            this.txtVoyage.TabIndex = 1;
            // 
            // txtShipname
            // 
            this.txtShipname.Location = new System.Drawing.Point(110, 24);
            this.txtShipname.Name = "txtShipname";
            this.txtShipname.Size = new System.Drawing.Size(216, 20);
            this.txtShipname.TabIndex = 2;
            // 
            // txtCallsign
            // 
            this.txtCallsign.Location = new System.Drawing.Point(434, 24);
            this.txtCallsign.Name = "txtCallsign";
            this.txtCallsign.Size = new System.Drawing.Size(226, 20);
            this.txtCallsign.TabIndex = 4;
            // 
            // DP1
            // 
            this.DP1.Location = new System.Drawing.Point(434, 3);
            this.DP1.Name = "DP1";
            this.DP1.Size = new System.Drawing.Size(226, 20);
            this.DP1.TabIndex = 3;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.txtLongitude, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTime, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label16, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtLatitude, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtRemarks, 3, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(239, 56);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(663, 44);
            this.tableLayoutPanel3.TabIndex = 8;
            // 
            // txtLongitude
            // 
            this.txtLongitude.Location = new System.Drawing.Point(168, 18);
            this.txtLongitude.Name = "txtLongitude";
            this.txtLongitude.Size = new System.Drawing.Size(159, 20);
            this.txtLongitude.TabIndex = 6;
            // 
            // txtTime
            // 
            this.txtTime.Location = new System.Drawing.Point(333, 18);
            this.txtTime.Name = "txtTime";
            this.txtTime.Size = new System.Drawing.Size(159, 20);
            this.txtTime.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(333, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(30, 13);
            this.label15.TabIndex = 0;
            this.label15.Text = "Time";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(45, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Latitude";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(168, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(54, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Longitude";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(498, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(49, 13);
            this.label16.TabIndex = 0;
            this.label16.Text = "Remarks";
            // 
            // txtLatitude
            // 
            this.txtLatitude.Location = new System.Drawing.Point(3, 18);
            this.txtLatitude.Name = "txtLatitude";
            this.txtLatitude.Size = new System.Drawing.Size(159, 20);
            this.txtLatitude.TabIndex = 5;
            // 
            // txtRemarks
            // 
            this.txtRemarks.Location = new System.Drawing.Point(498, 18);
            this.txtRemarks.Name = "txtRemarks";
            this.txtRemarks.Size = new System.Drawing.Size(162, 20);
            this.txtRemarks.TabIndex = 8;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label19, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label20, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtVSLHeading, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtCourseTime, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtRemarks1, 2, 1);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(239, 106);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(663, 42);
            this.tableLayoutPanel4.TabIndex = 10;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(81, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "Vessel Heading";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(224, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(30, 13);
            this.label19.TabIndex = 0;
            this.label19.Text = "Time";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(445, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Remarks";
            // 
            // txtVSLHeading
            // 
            this.txtVSLHeading.Location = new System.Drawing.Point(3, 18);
            this.txtVSLHeading.Name = "txtVSLHeading";
            this.txtVSLHeading.Size = new System.Drawing.Size(215, 20);
            this.txtVSLHeading.TabIndex = 9;
            // 
            // txtCourseTime
            // 
            this.txtCourseTime.Location = new System.Drawing.Point(224, 18);
            this.txtCourseTime.Name = "txtCourseTime";
            this.txtCourseTime.Size = new System.Drawing.Size(215, 20);
            this.txtCourseTime.TabIndex = 10;
            // 
            // txtRemarks1
            // 
            this.txtRemarks1.Location = new System.Drawing.Point(445, 18);
            this.txtRemarks1.Name = "txtRemarks1";
            this.txtRemarks1.Size = new System.Drawing.Size(215, 20);
            this.txtRemarks1.TabIndex = 11;
            // 
            // lblNoonReport
            // 
            this.lblNoonReport.AutoSize = true;
            this.lblNoonReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoonReport.Location = new System.Drawing.Point(3, 0);
            this.lblNoonReport.Name = "lblNoonReport";
            this.lblNoonReport.Size = new System.Drawing.Size(79, 13);
            this.lblNoonReport.TabIndex = 11;
            this.lblNoonReport.Text = "Noon Report";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "Noon Position";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(46, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Course";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel2.Controls.Add(this.lblCompanyName);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(911, 54);
            this.panel2.TabIndex = 2;
            // 
            // lblCompanyName
            // 
            this.lblCompanyName.AutoSize = true;
            this.lblCompanyName.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.lblCompanyName.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCompanyName.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.lblCompanyName.Location = new System.Drawing.Point(260, 10);
            this.lblCompanyName.Name = "lblCompanyName";
            this.lblCompanyName.Size = new System.Drawing.Size(287, 31);
            this.lblCompanyName.TabIndex = 0;
            this.lblCompanyName.Text = "Petrojaya Noon Report";
            // 
            // Petro_NoonReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(919, 544);
            this.Controls.Add(this.pnlPrint);
            this.Name = "Petro_NoonReports";
            this.Text = "Petro_NoonReports";
            this.pnlPrint.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.LoadNoonReport)).EndInit();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlPrint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBackIn;
        private System.Windows.Forms.Button btnPrint;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button loadbtn;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.TextBox txtLOMainengineSump;
        private System.Windows.Forms.TextBox txtLOMainengineStoragetank;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox txtBunkerReceivedMFO;
        private System.Windows.Forms.TextBox txtBunkerReceivedMGO;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TextBox txtBunkerRobMFO;
        private System.Windows.Forms.TextBox txtBunkerRobMGO;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox txtMGOmainengine;
        private System.Windows.Forms.TextBox txtMGOauxiliaryengine;
        private System.Windows.Forms.TextBox txtMGOboiler;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtMFOmainengine;
        private System.Windows.Forms.TextBox txtMFOboiler;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtAvgSpeed;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtVoyageSpeed;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtVoyage;
        private System.Windows.Forms.TextBox txtShipname;
        private System.Windows.Forms.TextBox txtCallsign;
        private System.Windows.Forms.DateTimePicker DP1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TextBox txtLongitude;
        private System.Windows.Forms.TextBox txtTime;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtLatitude;
        private System.Windows.Forms.TextBox txtRemarks;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtVSLHeading;
        private System.Windows.Forms.TextBox txtCourseTime;
        private System.Windows.Forms.TextBox txtRemarks1;
        private System.Windows.Forms.Label lblNoonReport;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCompanyName;
        private System.Drawing.Printing.PrintDocument printdocNoonReport;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label lblisUpdated;
        private System.Windows.Forms.PictureBox LoadNoonReport;
        private System.Windows.Forms.Button btn_reply;
        private System.Windows.Forms.Label lblMrgQMail;
        private System.Windows.Forms.Label label1;
    }
}