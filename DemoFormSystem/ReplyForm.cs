﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using EAGetMail;
using EASendMail;

namespace DemoFormSystem
{
    public partial class ReplyForm : Form
    {
        private static string _emailID { get; set; }

        private static string _password { get; set; }

        private static string _NoonEmailID { get; set; }

        private static string _NoonPassword { get; set; }

        public ReplyForm()
        {
            InitializeComponent();
            _emailID = Petro_MorningReports.replyTo;
            _password = Petro_MorningReports.replyCC;
            _NoonEmailID = Petro_NoonReports.ReplyTo;
            _NoonPassword = Petro_NoonReports.ReplyCC;
        }

        private void btn_replyclose_Click(object sender, EventArgs e)
        {
            if (Petro_NoonReports.isNoon == true)
            {
                Petro_NoonReports pnr = new Petro_NoonReports();
                this.Hide();
                pnr.Show();
                this.Close();
                
            }
            else
            {
                Petro_MorningReports pmr = new Petro_MorningReports();
                this.Hide();
                pmr.Show();
                this.Close();
                
            }
        }

        private void btn_replysend_Click(object sender, EventArgs e)
        {
            List<string> lstccmail = new List<string>();
            if (Petro_NoonReports.isNoon)
            {
                lstccmail.Add(Petro_NoonReports.ReplyCC);
                MessageBox.Show(Helper.SendEmail(formLogin.EmailID, formLogin.Password, Petro_NoonReports.ReplyTo, string.Empty, "Reply From Shore", txt_reply.Text, lstccmail, string.Empty));

            }
            else
            {
                lstccmail.Add(Petro_MorningReports.replyCC);
                MessageBox.Show(Helper.SendEmail(formLogin.EmailID, formLogin.Password, Petro_MorningReports.replyTo, string.Empty, "Reply From Shore", txt_reply.Text, lstccmail, string.Empty));
            }
            //Petro_MorningReports pm = new Petro_MorningReports();
            //pm.Show();

        }
    }
}
