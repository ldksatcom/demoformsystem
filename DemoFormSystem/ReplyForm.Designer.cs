﻿namespace DemoFormSystem
{
    partial class ReplyForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_reply = new System.Windows.Forms.TextBox();
            this.btn_replysend = new System.Windows.Forms.Button();
            this.btn_replyclose = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txt_reply
            // 
            this.txt_reply.Location = new System.Drawing.Point(109, 24);
            this.txt_reply.Multiline = true;
            this.txt_reply.Name = "txt_reply";
            this.txt_reply.Size = new System.Drawing.Size(263, 74);
            this.txt_reply.TabIndex = 0;
            // 
            // btn_replysend
            // 
            this.btn_replysend.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_replysend.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_replysend.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_replysend.Location = new System.Drawing.Point(143, 123);
            this.btn_replysend.Name = "btn_replysend";
            this.btn_replysend.Size = new System.Drawing.Size(75, 35);
            this.btn_replysend.TabIndex = 1;
            this.btn_replysend.Text = "Send";
            this.btn_replysend.UseVisualStyleBackColor = false;
            this.btn_replysend.Click += new System.EventHandler(this.btn_replysend_Click);
            // 
            // btn_replyclose
            // 
            this.btn_replyclose.BackColor = System.Drawing.SystemColors.Highlight;
            this.btn_replyclose.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_replyclose.ForeColor = System.Drawing.SystemColors.Window;
            this.btn_replyclose.Location = new System.Drawing.Point(297, 123);
            this.btn_replyclose.Name = "btn_replyclose";
            this.btn_replyclose.Size = new System.Drawing.Size(75, 35);
            this.btn_replyclose.TabIndex = 2;
            this.btn_replyclose.Text = "Close";
            this.btn_replyclose.UseVisualStyleBackColor = false;
            this.btn_replyclose.Click += new System.EventHandler(this.btn_replyclose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Reply";
            // 
            // ReplyForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.ClientSize = new System.Drawing.Size(384, 170);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_replyclose);
            this.Controls.Add(this.btn_replysend);
            this.Controls.Add(this.txt_reply);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "ReplyForm";
            this.Text = "ReplyForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_reply;
        private System.Windows.Forms.Button btn_replysend;
        private System.Windows.Forms.Button btn_replyclose;
        private System.Windows.Forms.Label label1;
    }
}