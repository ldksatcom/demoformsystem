﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DemoFormSystem
{
    public partial class AddEditConnections : Form
    {
        public static List<connection> connections { get; set; }
        public AddEditConnections()
        {
            InitializeComponent();

            txtIPAddress.Text = BAL.Terminal.getIPV4Addresslocalamchine();

            List<Item> items = new List<Item>();
            //items.Add(new Item() { Text = "Imarsat B", Value = "Imarsat B" });
            //items.Add(new Item() { Text = "Imarsat Mini-M", Value = "Imarsat Mini-M" });
            items.Add(new Item() { Text = "TCP/IP", Value = "TCP/IP" });
            items.Add(new Item() { Text = "Fleet BoardBand", Value = "Fleet BoardBand" });
            items.Add(new Item() { Text = "Iridium", Value = "Iridium" });
            items.Add(new Item() { Text = "Internet", Value = "Internet" });

            ddlConnectionType.DataSource = items;
            ddlConnectionType.DisplayMember = "Text";
            ddlConnectionType.ValueMember = "Value";

            BAL.Terminal terminal = new BAL.Terminal();
            List<string> lstTerminal = terminal["TCP/IP"];

            ddlTerminal.DataSource = lstTerminal;

            List<Item> itemsAPN = new List<Item>();
            itemsAPN.Add(new Item() { Text = "Default to SIM", Value = "Default to SIM" });
            itemsAPN.Add(new Item() { Text = "bgan.inmarsat.com", Value = "bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "stratos.bgan.inmarsat.com", Value = "stratos.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "broadband.singtel.bgan.inmarsat.com", Value = "broadband.singtel.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "ftmsc.bgan.inmarsat.com", Value = "ftmsc.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "xantic.bgan.inmarsat.com", Value = "xantic.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "kddi.bgan.inmarsat.com", Value = "kddi.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "711.bgan.inmarsat.com", Value = "711.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "satcom.bgan.inmarsat.com", Value = "satcom.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "mvs.bgan.inmarsat.com", Value = "mvs.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "otesat-maritel.fbb.bgan.inmarsat.com", Value = "otesat-maritel.fbb.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "broadband.bgan.inmarsat.com", Value = "broadband.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "vizada.bgan.inmarsat.com", Value = "vizada.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "sc.bgan.inmarsat.com", Value = "sc.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "srh.bgan.inmarsat.com", Value = "srh.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "onenet.bgan.inmarsat.com", Value = "onenet.bgan.inmarsat.com" });
            itemsAPN.Add(new Item() { Text = "ss.bgan.inmarsat.com", Value = "ss.bgan.inmarsat.com" });

            ddlAPN.DataSource = itemsAPN;
            ddlAPN.DisplayMember = "Text";
            ddlAPN.ValueMember = "Value";
        }

        private void ddlConnectionType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string conType = ddlConnectionType.SelectedValue.ToString();

            BAL.Terminal terminal = new BAL.Terminal();
            List<string> lstTerminal = terminal[conType];

            ddlTerminal.DataSource = lstTerminal;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            connections = new List<connection>();
             connection con = new connection();
            if(!string.IsNullOrEmpty(Convert.ToString(ddlConnectionType.SelectedValue)))
            {
                if(!string.IsNullOrEmpty(Convert.ToString(ddlTerminal.SelectedValue)))
                {
                    if(!string.IsNullOrEmpty(lblUsername.Text) && !string.IsNullOrEmpty(lblpassword.Text))
                    {
                        con.ID = 1;
                        con.ConnectionName = Convert.ToString(ddlConnectionType.SelectedValue);
                        con.Terminal = Convert.ToString(ddlTerminal.SelectedValue);
                        con.UserName = txtUsername.Text;
                        con.Password = txtPassword.Text;
                        con.IPAddress = txtIPAddress.Text;
                        con.IPPort = !string.IsNullOrEmpty(txtIPPort.Text)? Convert.ToInt32(txtIPPort.Text):0;
                        con.Mailbag = txtMailBag.Text;
                        con.Tatiff = txtTeriff.Text;

                        connections.Add(con);

                        Connections connectionsobj = new Connections();
                        connectionsobj.Show();
                        this.Close();
                    }
                    else
                    {
                        MessageBox.Show("The UserName or Password is incorect");
                    }
                }
                else
                {
                    MessageBox.Show("Please select the Terminal Type");
                    return;
                }
            }
            else
            {
                MessageBox.Show("Please select Connection Type");
                return;
            }
        }
    }

    public class Item
    {
        public Item() { }

        public string Value { set; get; }
        public string Text { set; get; }
    }
}
