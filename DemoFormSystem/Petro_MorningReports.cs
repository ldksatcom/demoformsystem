﻿using Compressed_Library;
using DemoFormSystem;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Petrojayaforminsert;
using EAGetMail;

namespace DemoFormSystem
{
    public partial class Petro_MorningReports : Form
    {
        public static string M_printFilelocation { get; set; }
        public static string M_Filenname { get; set; }
        public static string M_originalSize { get; set; }
        public static string M_CompressedByteSize { get; set; }
        public static string M_attachFileName { get; set; }
        public static string M_textFileSize { get; set; }
        public static string M_CompressedPercentage { get; set; }
        public static Int64 M_compressedLength { get; set; }
        public static string M_textFileLength { get; set; }
        public static System.IO.FileInfo M_fileinfoText { get; set; }

        public static string M_emailbodystring { get; set; }

        public static string M_EmailID { get; set; }
        public static string M_Password { get; set; }

        public static bool M_isShip { get; set; }
        public static string M_CompanyName { get; set; }
        public static string M_VesselName { get; set; }


        public static bool isMoring { get; set; }

        public static string replyTo { get; set; }
        public static string replyCC { get; set; }

        public static int M_ReportID { get; set; }

        public static int QueMail { get; set; }

        public static string localDBPath { get; set; }

        public Petro_MorningReports()
        {
            InitializeComponent();

            lblCompanyName.Text = M_CompanyName = formLogin.CompanyName + "   " +"Form System";

            txtShipname.Text = M_VesselName = formLogin.VesselName;

            txtShipname.ReadOnly = false;

            M_EmailID = formLogin.EmailID;

            M_Password = formLogin.Password;

            if (formLogin.Types.Equals("Ship"))
                M_isShip = true;
            else
                M_isShip = false;

            isMoring = true;
            Petro_NoonReports.isNoon = false;

            txtTime.Text = Convert.ToString(DateTime.Now.ToString("HH:mm"));

            txtCourseTime.Text = Convert.ToString(DateTime.Now.ToString("HH:mm"));

            //declare event handler for printing in constructor
            printdocMorning.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(prntDocument_PrintPage);
        }

        //Rest of the code
        Bitmap MemoryImage;
        public void GetPrintArea(Panel pnl)
        {
            MemoryImage = new Bitmap(pnl.Width, pnl.Height);
            pnl.DrawToBitmap(MemoryImage, new Rectangle(0, 0, pnl.Width, pnl.Height));
        }

        private void prntDocument_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Rectangle pagearea = e.PageBounds;
            //e.Graphics.DrawImage(MemoryImage, (pagearea.Width / 2) - (this.pnlMain.Width / 2), this.pnlMain.Location.Y);
            //e.Graphics.DrawImage(MemoryImage, pagearea.Width, this.pnlMain.Location.Y);
            e.Graphics.DrawImage(MemoryImage, pagearea);

            //float x = e.MarginBounds.Left;
            //float y = e.MarginBounds.Top;
            //e.Graphics.DrawImage(MemoryImage, x, y, 1000, 780);
        }

        public void Print(Panel pnl)
        {

            GetPrintArea(pnl);

            printdocMorning.DefaultPageSettings.Landscape = true;
            //printDialog.Document = prntDocument;
            //printDialog.ShowDialog();
            //prntDocument.Print();


            System.Windows.Forms.PrintDialog PrintDialog1 = new PrintDialog();
            PrintDialog1.AllowSomePages = true;
            PrintDialog1.ShowHelp = true;
            PrintDialog1.Document = printdocMorning;
            DialogResult result = PrintDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                printdocMorning.Print();
            }
        }

        private void btnBackIn_Click(object sender, EventArgs e)
        {
            DashBoard dashBoard = new DashBoard();
            this.Hide();
            dashBoard.Show();
            this.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnPrint_Click(object sender, EventArgs e)
        {
            Print(this.pnlPrint);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                btnSave.Enabled = false;

                #region Validation
                if (string.IsNullOrEmpty(txtVoyage.Text))
                {
                    MessageBox.Show("Voyage No field is required, please enter the valid name.");
                    return;
                }

                if ((!string.IsNullOrEmpty(txtLOMainengineSump.Text)))
                {
                    if (!Helper.Validated(txtLOMainengineSump.Text))
                    {
                        MessageBox.Show("LO Main Engine Sumb is required litres as type, please enter the type as litrs");
                        txtLOMainengineSump.Focus();
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(txtLOMainengineStoragetank.Text))
                {
                    if (!Helper.Validated(txtLOMainengineStoragetank.Text))
                    {
                        MessageBox.Show("LO Main Engine Stoage Tank is required litres as type, please enter the type as litrs");
                        txtLOMainengineStoragetank.Focus();
                        return;
                    }
                }
                #endregion

                string formDirctory = string.Empty;

                if(!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                    formDirctory = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
                else
                    formDirctory = @"C:\FormSystemReport";

                if (!Directory.Exists(formDirctory))
                    Directory.CreateDirectory(formDirctory);

                #region Save Form

                //DialogResult dialogResult = MessageBox.Show("Are you want to save this form, If yes please choose the location", "Information", MessageBoxButtons.YesNo);

                string tempPathForm = System.IO.Path.GetTempPath() + txtShipname.Text + "_" + Guid.NewGuid().ToString() + ".jpg";

                Bitmap bmp = new Bitmap(pnlPrint.ClientRectangle.Width, pnlPrint.ClientRectangle.Height);
                pnlPrint.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                bmp.Save(tempPathForm, System.Drawing.Imaging.ImageFormat.Png);

                string saveFormLoc = formDirctory;

                string pathID = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");
                saveFormLoc += "/" + txtShipname.Text + " " + pathID + ".pdf";

                // Create new pdf document and page
                PdfDocument doc = new PdfDocument();
                PdfPage oPage = new PdfPage();

                // Add the page to the pdf document and add the captured image to it
                doc.Pages.Add(oPage);
                doc.PageMode = PdfPageMode.FullScreen;

                oPage.Width = pnlPrint.ClientRectangle.Width;
                oPage.Height = pnlPrint.ClientRectangle.Height;
                PdfSharp.Drawing.XGraphics xgr = PdfSharp.Drawing.XGraphics.FromPdfPage(oPage);
                PdfSharp.Drawing.XImage img = PdfSharp.Drawing.XImage.FromFile(tempPathForm);
                xgr.DrawImage(img, 0, 0);

                doc.Save(saveFormLoc);
                doc.Close();
                M_printFilelocation = saveFormLoc;
                img.Dispose();

                //if (dialogResult == DialogResult.Yes)
                //{
                //    using (FolderBrowserDialog opnDialog = new FolderBrowserDialog())
                //    {
                //        if (opnDialog.ShowDialog() == DialogResult.OK)
                //        {
                //            string testpath = opnDialog.SelectedPath;
                //            ExcelPath = testpath;
                //            string pathID = Guid.NewGuid().ToString()+ " " + DateTime.Now.Date.ToString("dd-MM-yyyy");
                //            testpath += "/" + txtShipname.Text +" "+ pathID + ".pdf";
                //            //testpath += "/" + txtShipname.Text + ".jpg";
                //            //Bitmap bmp = new Bitmap(pnlMain.ClientRectangle.Width, pnlMain.ClientRectangle.Height);
                //            //pnlMain.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                //            //bmp.Save(testpath, ImageFormat.Png);


                //        }
                //    }

                //}
                //else if (dialogResult == DialogResult.No)
                //{
                //    //do something else
                //}
                #endregion

                string strData = string.Empty;

                #region Save to Database
                try
                {
                    string ExcelPath = formDirctory;
                    ExcelPath += "\\ReportData.xlsx";

                    var tupleData = Helper.SavetoDB(getFormData(), ExcelPath);
                    strData = tupleData.Item1;
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message.ToString());
                }
                #endregion

                #region Work Email 

                byte[] bufferOriginal = Encoding.ASCII.GetBytes(strData);
                M_originalSize = BytestoKB.SizeSuffix(bufferOriginal.Length);
                string tempPath = System.IO.Path.GetTempPath() + txtShipname.Text + "_" + Guid.NewGuid().ToString() + ".txt";

                //byte[] comp1 = Compress_Logic.CompressLogic1(bufferOriginal);
                //string basestring = Convert.ToBase64String(comp1);
                File.WriteAllText(tempPath, strData);

                M_fileinfoText = new FileInfo(tempPath);
                M_textFileLength = BytestoKB.SizeSuffix(M_fileinfoText.Length);

                tempPath = Path.GetTempPath() + txtShipname.Text + "_" + Guid.NewGuid().ToString() + ".txt";

                string returnstring = Compress_DeCompress.CompressString(strData.ToString());
                File.WriteAllText(tempPath, returnstring);

                M_emailbodystring = returnstring;

                byte[] buffer = Convert.FromBase64String(returnstring);
                Int64 bufferSize = buffer.Length;
                M_CompressedByteSize = BytestoKB.SizeSuffix(bufferSize);

                FileInfo file = new FileInfo(tempPath);
                M_attachFileName = Path.GetFileNameWithoutExtension(file.FullName);
                M_Filenname = file.FullName;
                M_textFileSize = BytestoKB.SizeSuffix(file.Length);

                M_compressedLength = ((byte[])Encoding.ASCII.GetBytes(strData)).Length - bufferSize;
                M_CompressedPercentage = BytestoKB.GetPercentage(bufferOriginal.Length, M_compressedLength);

                //EmailForm emailForm = new Petrojayaforminsert.EmailForm();
                //emailForm.Tag = this;
                //emailForm.Show(this);
                //Hide();

                Connections connections = new Connections();
                connections.Tag = this;
                connections.Show(this);
                Hide();

                #endregion
            }
            catch (Exception ex)
            {
                btnSave.Enabled = true;
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private Dictionary<string, dynamic> getFormData(string[] data = null)
        {
            bool rec = false;
            if (data != null && data.Length > 1)
                rec = true;
            Dictionary<string, dynamic> dicNoonReport = new Dictionary<string, dynamic>();

            dicNoonReport.Add("ReportType", rec ? data[0] : "Morning");

            if (!string.IsNullOrEmpty(lblisUpdated.Text) && lblisUpdated.Text.Equals("1"))
                dicNoonReport.Add("IsUpdated", "Yes");
            else
                dicNoonReport.Add("IsUpdated", "No");

            if (!string.IsNullOrEmpty(txtVoyage.Text) || rec)
                dicNoonReport.Add("VoyageNo", rec ? data[1] : txtVoyage.Text);
            if (!string.IsNullOrEmpty(txtShipname.Text) || rec)
                dicNoonReport.Add("ShipName", rec ? data[2] : txtShipname.Text);
            if (!string.IsNullOrEmpty(DP1.Text) || rec)
                dicNoonReport.Add("ReportDate", rec ? data[3] : Convert.ToDateTime(DP1.Text).ToString("dd/MM/yyyy"));

            if (!string.IsNullOrEmpty(txtCallsign.Text) || rec)
                dicNoonReport.Add("CallSign", rec ? data[4] : txtCallsign.Text);

            if (!string.IsNullOrEmpty(txtLatitude.Text) || rec)
                dicNoonReport.Add("Latitude", rec ? data[5] : txtLatitude.Text);

            if (!string.IsNullOrEmpty(txtLongitude.Text) || rec)
                dicNoonReport.Add("Longitude", rec ? data[6] : txtLongitude.Text);

            if (!string.IsNullOrEmpty(txtTime.Text) || rec)
                dicNoonReport.Add("NPTime", rec ? data[7] : txtTime.Text);

            if (!string.IsNullOrEmpty(txtRemarks.Text) || rec)
                dicNoonReport.Add("Remarks", rec ? data[8] : txtRemarks.Text);

            if (!string.IsNullOrEmpty(txtVSLHeading.Text) || rec)
                dicNoonReport.Add("VesselHeading", rec ? data[9] : txtVSLHeading.Text);

            if (!string.IsNullOrEmpty(txtCourseTime.Text) || rec)
                dicNoonReport.Add("CTime", rec ? data[10] : txtCourseTime.Text);

            if (!string.IsNullOrEmpty(txtRemarks1.Text) || rec)
                dicNoonReport.Add("Remarks1", rec ? data[11] : txtRemarks1.Text);

            if (!string.IsNullOrEmpty(txtAvgSpeed.Text) || rec)
                dicNoonReport.Add("AverageSpeed_At_Noons", rec ? data[12] : txtAvgSpeed.Text);

            if (!string.IsNullOrEmpty(txtVoyageSpeed.Text) || rec)
                dicNoonReport.Add("Forvoyagesincelastfullaway", rec ? data[13] : txtVoyageSpeed.Text);

            if (!string.IsNullOrEmpty(txtMFOmainengine.Text) || rec)
                dicNoonReport.Add("ForMainEngine", rec ? data[14] : txtMFOmainengine.Text);

            if (!string.IsNullOrEmpty(txtMFOboiler.Text) || rec)
                dicNoonReport.Add("ForBoiler", rec ? data[15] : txtMFOboiler.Text);

            if (!string.IsNullOrEmpty(txtMGOmainengine.Text) || rec)
                dicNoonReport.Add("ForMainEngine1", rec ? data[16] : txtMGOmainengine.Text);

            if (!string.IsNullOrEmpty(txtMGOauxiliaryengine.Text) || rec)
                dicNoonReport.Add("ForAuxiliaryEngines", rec ? data[17] : txtMGOauxiliaryengine.Text);

            if (!string.IsNullOrEmpty(txtMGOboiler.Text) || rec)
                dicNoonReport.Add("ForBoiler1", rec ? data[18] : txtMGOboiler.Text);

            if (!string.IsNullOrEmpty(txtMGOmainengine.Text) || rec)
                dicNoonReport.Add("TotalMFO", rec ? data[19] : txtMGOmainengine.Text);

            if (!string.IsNullOrEmpty(txtBunkerRobMGO.Text) || rec)
                dicNoonReport.Add("TotalMGO", rec ? data[20] : txtBunkerRobMGO.Text);

            if (!string.IsNullOrEmpty(txtBunkerReceivedMFO.Text) || rec)
                dicNoonReport.Add("MFO", rec ? data[21] : txtBunkerReceivedMFO.Text);

            if (!string.IsNullOrEmpty(txtBunkerReceivedMGO.Text) || rec)
                dicNoonReport.Add("MGO", rec ? data[22] : txtBunkerReceivedMGO.Text);

            if ((!string.IsNullOrEmpty(txtLOMainengineSump.Text)) || rec)
                dicNoonReport.Add("MainEnginesystem_Sump", rec ? data[23] : txtLOMainengineSump.Text);

            if (!string.IsNullOrEmpty(txtLOMainengineStoragetank.Text) || rec)
                dicNoonReport.Add("MainEnginesystem_StorageTank", rec ? data[24] : txtLOMainengineStoragetank.Text);

            return dicNoonReport;
        }

        private void loadbtn_Click(object sender, EventArgs e)
        {
            this.Disablebuttons(false);

            btnUpdate.Enabled = true;

            string DecompressedData = string.Empty;

            string formDirctory = string.Empty;

            if (!string.IsNullOrEmpty(formLogin.CompanyName) && !string.IsNullOrEmpty(formLogin.VesselName))
                formDirctory = @"C:\" + formLogin.CompanyName + "\\" + formLogin.VesselName;
            else
                formDirctory = @"C:\FormSystem";

            if (!Directory.Exists(formDirctory))
                Directory.CreateDirectory(formDirctory);

            // Hotmail/Outlook IMAP4 server is "imap-mail.outlook.com"
            MailServer oServer = new MailServer("mail.ldksatcom.in",
                        M_EmailID, M_Password, ServerProtocol.Imap4);

            MailClient oClient = new MailClient("TryIt");

            // Set SSL connection
            oServer.SSLConnection = false;

            // Set IMAP4 SSL port
            oServer.Port = 143;

            //ReadMailItems();
            try
            {
                oClient.Connect(oServer);

                Imap4Folder[] folders = oClient.Imap4Folders;
                int count = folders.Length;

                for (int i = 0; i < count; i++)
                {
                    Imap4Folder folder = folders[i];
                    if (String.Compare("Morning", folder.Name, true) == 0)
                    {
                        //select "ldksatcom" folder
                        oClient.SelectFolder(folder);
                        break;
                    }
                }

                // get emails information in current selected folder.
                MailInfo[] infos = oClient.GetMailInfos();

                //Console.WriteLine("{0} mailbox has {1} emails", oClient.SelectedFolder, infos.Length);

                int coundMail = infos.Length;
                if (coundMail > 0)
                {
                    QueMail = coundMail - 1;
                    lblNoonQMail.Text = Convert.ToString(QueMail);
                    MailInfo info = new MailInfo();
                    try
                    {
                        info = infos[0];
                    }
                    catch
                    {
                        MessageBox.Show("No Reports are received from Ship/Shore, Try again another time.");
                        return;
                    }

                    // Receive email from IMAP4 server
                    Mail oMail = oClient.GetMail(info);

                    string from = oMail.From.ToString();
                    string sub = oMail.Subject;

                    if (oMail.Subject.ToString().Contains("Morning_Report") && !oMail.Subject.ToString().Contains("Reply From Shore"))
                    {
                        MailAddress mailFrom = oMail.From;
                        MailAddress mailTo = oMail.From;
                        MailAddress[] mailCC = oMail.Cc;

                        replyTo = mailTo.ToString();

                        if (mailCC.Count() > 0)
                        {
                            replyCC = string.Empty;
                            foreach (MailAddress mailAddress in mailCC)
                            {
                                if (string.IsNullOrEmpty(replyCC))
                                    replyCC = mailAddress.Address;
                                else
                                    replyCC += ";" + mailAddress.Address;
                            }
                        }

                        string textbody = oMail.TextBody;
                        string htmlbody = oMail.HtmlBody;
                        HeaderCollection bodyHeader = oMail.BodyHeaders;
                        BodyTextFormat bodyFormat = oMail.OriginalBodyFormat;
                        DecompressedData = Helper.DecompressString(textbody);

                        if (!string.IsNullOrEmpty(DecompressedData) && DecompressedData.Contains(","))
                        {
                            string a = string.Empty;
                            string[] arrData = DecompressedData.Split(',');
                            Dictionary<string, dynamic> dictFromData = getFormData(arrData);

                            BindForm(dictFromData);

                            #region Save Form
                            //DialogResult dialogResult = MessageBox.Show("Please choose the reporting dirctory for form save and excel save location, if not choose location file not proceed.", "Information", MessageBoxButtons.YesNo);

                            string tempPathForm = System.IO.Path.GetTempPath() + txtShipname.Text + "_" + Guid.NewGuid().ToString() + ".jpg";

                            Bitmap bmp = new Bitmap(pnlPrint.ClientRectangle.Width, pnlPrint.ClientRectangle.Height);
                            pnlPrint.DrawToBitmap(bmp, new Rectangle(0, 0, bmp.Width, bmp.Height));
                            bmp.Save(tempPathForm, System.Drawing.Imaging.ImageFormat.Png);

                            string ExcelPath = string.Empty;

                            string saveFormLoc = formDirctory;

                            string pathID = Guid.NewGuid().ToString() + " " + DateTime.Now.Date.ToString("dd-MM-yyyy");
                            saveFormLoc += "/" + txtShipname.Text + " " + pathID + ".pdf";

                            // Create new pdf document and page
                            PdfDocument doc = new PdfDocument();
                            PdfPage oPage = new PdfPage();

                            // Add the page to the pdf document and add the captured image to it
                            doc.Pages.Add(oPage);
                            doc.PageMode = PdfPageMode.FullScreen;

                            oPage.Width = pnlPrint.ClientRectangle.Width;
                            oPage.Height = pnlPrint.ClientRectangle.Height;
                            PdfSharp.Drawing.XGraphics xgr = PdfSharp.Drawing.XGraphics.FromPdfPage(oPage);
                            PdfSharp.Drawing.XImage img = PdfSharp.Drawing.XImage.FromFile(tempPathForm);
                            xgr.DrawImage(img, 0, 0);

                            doc.Save(saveFormLoc);
                            doc.Close();
                            M_printFilelocation = saveFormLoc;
                            img.Dispose();

                            //this.txtreply.Visible = true;
                            //this.btnreply.Visible = true;
                            //this.lbl_reply.Visible = true;
                            #endregion

                            ExcelPath = formDirctory;
                            ExcelPath += "\\ReportData.xlsx";

                            localDBPath = ExcelPath;

                            var tupledata = Helper.SavetoDB(getFormData(), ExcelPath);

                            string strdata = tupledata.Item1;

                            this.disableFields(true);

                            this.LoadingImage.Visible = false;

                            #region Attachments   
                            try
                            {
                                EAGetMail.Attachment[] attach = oMail.Attachments;
                                if (attach.Length > 0)
                                {
                                    byte[] buffer = attach[0].Content;
                                    string fileName = attach[0].Name;
                                    string contentType = attach[0].ContentType;

                                    string code = Encoding.UTF8.GetString(buffer);

                                    string storeAttachFile = formDirctory + "\\" + fileName;
                                    File.WriteAllBytes(formDirctory, buffer);
                                    ExportExcel.addAttachfileToexcel(formDirctory + "\\ReportData.xlsx", storeAttachFile);
                                    //Helper.StoreFileToDatabase(fileName, tupledata.Item2, buffer, contentType);
                                }
                            }
                            catch (Exception error)
                            { }
                            #endregion

                            #region Move to ldksatcom Folder

                            Imap4Folder LDKFolder = null;
                            Imap4Folder[] ldkfolder = oClient.Imap4Folders;
                            int countFolder = folders.Length;

                            for (int j = 0; j < countFolder; j++)
                            {
                                Imap4Folder folder = folders[j];
                                if (String.Compare("ldksatcom", folder.Name, true) == 0)
                                {
                                    //find "Deleted Items" folder
                                    LDKFolder = folder;
                                    break;
                                }
                            }

                            if (LDKFolder != null)
                            {
                                oClient.Move(info, LDKFolder);
                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();
                            }
                            else
                            {
                                Imap4Folder folder = oClient.CreateFolder(null, "ldksatcom");
                                oClient.Move(info, folder);

                                // Mark email as deleted from POP3 server.
                                oClient.Delete(info);

                                // Quit and purge emails marked as deleted from Hotmail/MSN Live server.
                                oClient.Quit();
                            }

                            #endregion
                        }
                    }

                    lblNoonQMail.Text = string.Format("{0} are Queue, Please click to Load button to View", Convert.ToString(QueMail));
                    if (QueMail > 0)
                        loadbtn.Enabled = true;
                }

                #region OldCode 
                //MailInfo[] infos = oClient.GetMailInfos();
                //int infosLength = infos.Length;
                //for (int i = infos.Length - 1; i < infosLength; i--)
                //{
                //    MailInfo info = infos[i];

                //    // Download email from Hotmail/MSN IMAP4 server
                //    Mail oMail = oClient.GetMail(info);

                //    if (oMail.Subject.ToString().Contains("Morning_Report") && !oMail.Subject.ToString().Contains("Reply From Shore"))
                //    {
                //       // MailAddress  mailFrom = oMail.From;
                //        MailAddress mailTo = oMail.From;
                //        MailAddress[] mailCC = oMail.Cc;

                //        replyTo = mailTo.ToString();

                //        //if (mailTo.Count() > 0)
                //        //{
                //        //    replyTo = string.Empty;
                //        //    string prevTo = string.Empty;
                //        //    foreach (MailAddress mailAddress in mailCC)
                //        //    {
                //        //        if (prevTo != mailAddress.Address)
                //        //        {
                //        //            if (string.IsNullOrEmpty(replyTo))
                //        //            {
                //        //                replyTo = prevTo = mailAddress.Address;
                //        //            }
                //        //            else
                //        //            {
                //        //                replyTo = ";" + mailAddress.Address;
                //        //                prevTo = mailAddress.Address;
                //        //            }
                //        //        }
                //        //    }
                //        //}

                //        if (mailCC.Count() > 0)
                //        {
                //            replyCC = string.Empty;
                //            foreach(MailAddress mailAddress in mailCC)
                //            {
                //                if(string.IsNullOrEmpty(replyCC))
                //                    replyCC = mailAddress.Address;
                //                else
                //                    replyCC += ";" + mailAddress.Address;
                //            }
                //        }

                //        string textbody = oMail.TextBody;
                //        string htmlbody = oMail.HtmlBody;
                //        HeaderCollection bodyHeader = oMail.BodyHeaders;
                //        BodyTextFormat bodyFormat = oMail.OriginalBodyFormat;
                //        DecompressedData = Helper.DecompressString(textbody);

                //        //EAGetMail.Attachment[] attach = oMail.Attachments;
                //        //byte[] buffer = attach[0].Content;
                //        //try
                //        //{
                //        //    string code = Encoding.UTF8.GetString(buffer);
                //        //    DecompressedData = Helper.DecompressString(code);
                //        //}
                //        //catch (Exception error)
                //        //{
                //        //    MessageBox.Show(error.Message);
                //        //}
                //        // Generate an email file name based on date time.
                //        System.DateTime d = System.DateTime.Now;
                //        System.Globalization.CultureInfo cur = new
                //            System.Globalization.CultureInfo("en-US");
                //        string sdate = d.ToString("yyyyMMddHHmmss", cur);
                //        //string fileName = String.Format("{0}\\{1}{2}{3}.eml",
                //        //    mailbox, sdate, d.Millisecond.ToString("d3"), i);

                //        // Save email to local disk
                //        //oMail.SaveAs(fileName, true);

                //        //if (attach.Length > 0)
                //        //{
                //        //    // Mark email as deleted in Hotmail/MSN Live account.
                //        //    //oClient.Delete(info);
                //        //}
                //        break;
                //    }
                //}
                #endregion
            }
            catch (System.Exception ep)
            {
                MessageBox.Show(ep.Message);
            }
        }

        public void BindForm(Dictionary<string, dynamic> dictFromData, bool isupdate = false)
        {
            if (isupdate)
            {
                lblisUpdated.Text = "1";
                lblisUpdated.Visible = false;
            }

            txtVoyage.Text = dictFromData["VoyageNo"];

            if (dictFromData.ContainsKey("ShipName") && !string.IsNullOrEmpty(dictFromData["ShipName"]))
                txtShipname.Text = dictFromData["ShipName"];
            else
                txtShipname.Text = string.Empty;

            if (dictFromData.ContainsKey("ReportDate") && !string.IsNullOrEmpty(dictFromData["ReportDate"]))
                DP1.Text = dictFromData["ReportDate"];
            else
                DP1.Text = string.Empty;

            if (dictFromData.ContainsKey("CallSign") && !string.IsNullOrEmpty(dictFromData["CallSign"]))
                txtCallsign.Text = dictFromData["CallSign"];
            else
                txtCallsign.Text = string.Empty;

            if (dictFromData.ContainsKey("Latitude") && !string.IsNullOrEmpty(dictFromData["Latitude"]))
                txtLatitude.Text = dictFromData["Latitude"];
            else
                txtLatitude.Text = string.Empty;

            if (dictFromData.ContainsKey("Longitude") && !string.IsNullOrEmpty(dictFromData["Longitude"]))
                txtLongitude.Text = dictFromData["Longitude"];
            else
                txtLongitude.Text = string.Empty;

            if (dictFromData.ContainsKey("NPTime") && !string.IsNullOrEmpty(Convert.ToString(dictFromData["NPTime"])))
                txtTime.Text = Convert.ToString(dictFromData["NPTime"]);
            else
                txtTime.Text = string.Empty;

            if (dictFromData.ContainsKey("Remarks") && !string.IsNullOrEmpty(dictFromData["Remarks"]))
                txtRemarks.Text = dictFromData["Remarks"];
            else
                txtRemarks.Text = string.Empty;

            if (dictFromData.ContainsKey("VesselHeading") && !string.IsNullOrEmpty(dictFromData["VesselHeading"]))
                txtVSLHeading.Text = dictFromData["VesselHeading"];
            else
                txtVSLHeading.Text = string.Empty;

            if (dictFromData.ContainsKey("CTime") && !string.IsNullOrEmpty(Convert.ToString(dictFromData["CTime"])))
                txtCourseTime.Text = Convert.ToString(dictFromData["CTime"]);
            else
                txtCourseTime.Text = string.Empty;

            if (dictFromData.ContainsKey("Remarks1") && !string.IsNullOrEmpty(dictFromData["Remarks1"]))
                txtRemarks1.Text = dictFromData["Remarks1"];
            else
                txtRemarks1.Text = string.Empty;

            if (dictFromData.ContainsKey("AverageSpeed_At_Noons") && !string.IsNullOrEmpty(dictFromData["AverageSpeed_At_Noons"]))
                txtAvgSpeed.Text = dictFromData["AverageSpeed_At_Noons"];
            else
                txtAvgSpeed.Text = string.Empty;

            if (dictFromData.ContainsKey("Forvoyagesincelastfullaway") && !string.IsNullOrEmpty(dictFromData["Forvoyagesincelastfullaway"]))
                txtVoyageSpeed.Text = dictFromData["Forvoyagesincelastfullaway"];
            else
                txtVoyageSpeed.Text = string.Empty;

            if (dictFromData.ContainsKey("ForMainEngine") && !string.IsNullOrEmpty(dictFromData["ForMainEngine"]))
                txtMFOmainengine.Text = dictFromData["ForMainEngine"];
            else
                txtMFOmainengine.Text = string.Empty;

            if (dictFromData.ContainsKey("ForBoiler") && !string.IsNullOrEmpty(dictFromData["ForBoiler"]))
                txtMFOboiler.Text = dictFromData["ForBoiler"];
            else
                txtMFOboiler.Text = string.Empty;

            if (dictFromData.ContainsKey("ForMainEngine1") && !string.IsNullOrEmpty(dictFromData["ForMainEngine1"]))
                txtMGOmainengine.Text = dictFromData["ForMainEngine1"];
            else
                txtMGOmainengine.Text = string.Empty;

            if (dictFromData.ContainsKey("ForAuxiliaryEngines") && !string.IsNullOrEmpty(dictFromData["ForAuxiliaryEngines"]))
                txtMGOauxiliaryengine.Text = dictFromData["ForAuxiliaryEngines"];
            else
                txtMGOauxiliaryengine.Text = string.Empty;

            if (dictFromData.ContainsKey("ForBoiler1") && !string.IsNullOrEmpty(dictFromData["ForBoiler1"]))
                txtMGOboiler.Text = dictFromData["ForBoiler1"];
            else
                txtMGOboiler.Text = string.Empty;

            if (dictFromData.ContainsKey("TotalMFO") && !string.IsNullOrEmpty(dictFromData["TotalMFO"]))
                txtBunkerReceivedMFO.Text = dictFromData["TotalMFO"];
            else
                txtBunkerReceivedMFO.Text = string.Empty;

            if (dictFromData.ContainsKey("TotalMGO") && !string.IsNullOrEmpty(dictFromData["TotalMGO"]))
                txtBunkerRobMGO.Text = dictFromData["TotalMGO"];
            else
                txtBunkerRobMGO.Text = string.Empty;

            if (dictFromData.ContainsKey("MFO") && !string.IsNullOrEmpty(dictFromData["MFO"]))
                txtBunkerReceivedMFO.Text = dictFromData["MFO"];
            else
                txtBunkerReceivedMFO.Text = string.Empty;

            if (dictFromData.ContainsKey("MGO") && !string.IsNullOrEmpty(dictFromData["MGO"]))
                txtBunkerReceivedMGO.Text = dictFromData["MGO"];
            else
                txtBunkerReceivedMGO.Text = string.Empty;

            if (dictFromData.ContainsKey("MainEnginesystem_Sump") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_Sump"]))
                txtLOMainengineSump.Text = Convert.ToString(dictFromData["MainEnginesystem_Sump"]);
            else
                txtLOMainengineSump.Text = string.Empty;

            if (dictFromData.ContainsKey("MainEnginesystem_StorageTank") && !string.IsNullOrEmpty(dictFromData["MainEnginesystem_StorageTank"]))
                txtLOMainengineStoragetank.Text = Convert.ToString(dictFromData["MainEnginesystem_StorageTank"]);
            else
                txtLOMainengineStoragetank.Text = string.Empty;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            isMoring = true;
            gridReports gridReports = new gridReports();
            gridReports.ShowDialog();
            this.Close();
        }

        private void Disablebuttons(bool yes)
        {
            LoadingImage.Visible = yes ? false : true;
            loadbtn.Enabled = yes;
            btnSave.Enabled = yes;
            btnUpdate.Enabled = yes;
            btnPrint.Enabled = yes;

        }

        private void disableFields(bool yes)
        {
            txtVoyage.ReadOnly = yes;
            DP1.Enabled = false;
            txtShipname.ReadOnly = yes;
            txtAvgSpeed.ReadOnly = yes;
            txtBunkerReceivedMFO.ReadOnly = yes;
            txtBunkerReceivedMGO.ReadOnly = yes;
            txtBunkerRobMFO.ReadOnly = yes;
            txtBunkerRobMGO.ReadOnly = yes;
            txtCallsign.ReadOnly = yes;
            txtCourseTime.ReadOnly = yes;
            txtLatitude.ReadOnly = yes;
            txtLOMainengineStoragetank.ReadOnly = yes;
            txtLOMainengineSump.ReadOnly = yes;
            txtLongitude.ReadOnly = yes;
            txtMFOboiler.ReadOnly = yes;
            txtMFOmainengine.ReadOnly = yes;
            txtMGOauxiliaryengine.ReadOnly = yes;
            txtMGOboiler.ReadOnly = yes;
            txtMGOmainengine.ReadOnly = yes;
            txtRemarks.ReadOnly = yes;
            txtRemarks1.ReadOnly = yes;
            txtTime.ReadOnly = yes;
            txtVoyageSpeed.ReadOnly = yes;
            txtVSLHeading.ReadOnly = yes;



        }

        private void btn_Reply_Click(object sender, EventArgs e)
        {
            ReplyForm RF = new ReplyForm();
            RF.Show();
            this.Hide();
            this.Close();
        }
    }
}
